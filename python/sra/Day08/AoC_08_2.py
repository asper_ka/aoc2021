# -*- coding: utf-8 -*-
import re

text = open("input_test.txt").read()
text = open("input.txt").read()

lines = text.split('\n')
while lines[-1] == "":
    lines = lines[:-1]

digits = {}
digits[0] = set("abcefg")
digits[1] = set("cf")
digits[2] = set("acdeg")
digits[3] = set("acdfg")
digits[4] = set("bcdf")
digits[5] = set("abdfg")
digits[6] = set("abdefg")
digits[7] = set("acf")
digits[8] = set("abcdefg")
digits[9] = set("abcdfg")

sum = 0
for l in lines:
    mapping = {}
    l2 = l.split(' | ')[1].split(' ')
    l1 = l.split(' | ')[0].split(' ')
    sets = []
    pos = [0] * 11
    for d in range(len(l1)):
        sets.append(set(l1[d]))
        if len(l1[d]) == 2:
            pos[1] = d
        if len(l1[d]) == 4:
            pos[4] = d
        if len(l1[d]) == 3:
            pos[7] = d
        if len(l1[d]) == 7:
            pos[8] = d

    # difference zwischen 7 und 1 ergibt das erste Element a
    mapping['a'] = list(sets[pos[7]].difference(sets[pos[1]]))[0]

    # c unf f sidn beide in 1, c gibt's 8mal, f 9mal
    c = l1[pos[1]][0]
    n = 0
    for s in sets:
        if c in s:
            n+=1
    if n==9:
        mapping['c'] = l1[pos[1]][1]
        mapping['f'] = c
    else:
        mapping['f'] = l1[pos[1]][1]
        mapping['c'] = c

    # b und d unterscheiden sich zwischen 4 und 1
    c4 = list(sets[pos[4]].difference(sets[pos[1]]))
    n = 0
    # b gibt's 6 und d 7 mal
    for s in sets:
        if c4[0] in s:
            n+=1
    if n==6:
        mapping['b'] = c4[0]
        mapping['d'] = c4[1]
    else:
        mapping['b'] = c4[1]
        mapping['d'] = c4[0]

    # 9 hat länge 6 und die selben Elemente wie 4, e ist dann der UNterschied zu 8
    for s in sets:
        if len(s) == 6 and sets[pos[4]].intersection(s) == sets[pos[4]]:
            mapping['e'] = list(sets[pos[8]].difference(s))[0]

    # g ist dann das letzte Segment
    mapping['g'] = list(set("abcdefg").difference(set(mapping.values())))[0]
    cmapping = dict((v,k) for k,v in mapping.items())

    result = ""
    for n in l2:
        t = ""
        for c in n:
            t += cmapping[c]
        #print (t)
        for d in digits:
            #print(set(t), digits[d])
            if set(t) == digits[d]:
                result += str(d)
    print (int(result))
    sum += int(result)

print (sets)
print (pos)
print (mapping)
print (set(mapping.values()))
print (sum)
