# -*- coding: utf-8 -*-
import re

text = "3,4,3,1,2"
text = open("input.txt").read()

nrs_text = text.split(',')
nrs = {0:0, 1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0}
for n in nrs_text:
    nrs[int(n)] += 1
print (nrs)

for d in range(256):
    newnrs={}
    for i in range(8):
        newnrs[i] = nrs[i+1]
    newnrs[8] = nrs[0]
    newnrs[6] += nrs[0]
    nrs = newnrs
    #print (nrs)

sum =0
for i in nrs:
    sum += nrs[i]
print (sum)