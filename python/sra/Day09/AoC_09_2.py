# -*- coding: utf-8 -*-
import re

text = open("input_test.txt").read()
text = open("input.txt").read()

lines = text.split('\n')
while lines[-1] == "":
    lines = lines[:-1]

maxx= len(lines[0])
maxy = len(lines)
grid={}
y=0
for l in lines:
    for x in range (len(lines[0])):
        grid[(x,y)]=int(l[x])
    y += 1
#print(grid)

dist=[(-1,0), (1,0), (0,1), (0,-1)]

inBasin = {}
basinSizes = []

for (gx,gy) in grid:
    if not (gx,gy) in inBasin and grid[(gx,gy)]!=9:
        #print(gx,gy)
        size = 1
        queue = []
        visited = []
        queue.append((gx,gy))
        visited.append((gx,gy))
        inBasin[gx,gy] = True
        while queue:
            (cx,cy) = queue.pop(0)
            for (dx,dy) in dist:
                x = cx + dx
                y = cy + dy
                if (x, y) in grid and (grid[(x,y)]!=9) and (not (x,y) in visited):
                    #print(x,y)
                    size += 1
                    queue.append((x,y))
                    visited.append((x,y))
                    inBasin[(x,y)] = True
        #print(size)
        print()
        basinSizes.append(size)

basinSizes.sort()
print (basinSizes[-1] * basinSizes[-2] * basinSizes[-3])
