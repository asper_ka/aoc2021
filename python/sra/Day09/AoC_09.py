# -*- coding: utf-8 -*-
import re

text = open("input_test.txt").read()
text = open("input.txt").read()

lines = text.split('\n')
while lines[-1] == "":
    lines = lines[:-1]

maxx= len(lines[0])
maxy = len(lines)
grid={}
y=0
for l in lines:
    for x in range (len(lines[0])):
        grid[(x,y)]=int(l[x])
    y += 1
#print(grid)

dist=[(-1,0), (1,0), (0,1), (0,-1)]

sum = 0
for (gx,gy) in grid:
    low = True
    for (dx,dy) in dist:
        x = gx+dx
        y = gy+dy
        if (x,y) in grid:
            if (grid[(x,y)])<=grid[(gx,gy)]:
                low = False
    if low:
        print (gx, gx, grid[(gx, gy)])
        sum += 1+grid[(gx, gy)]

print (sum)