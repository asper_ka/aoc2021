# -*- coding: utf-8 -*-

text = open("input_test.txt").read()
text = open("input.txt").read()

lines = text.split('\n')
while lines[-1] == "":
    lines = lines[:-1]

def countOnes(ls, pos):
    c = 0
    for l in ls:
        if l[pos] == '1':
            c+=1
    return c

def filterLines(ls, pos, c):
    nls = []
    for l in ls:
        if l[pos] == c:
            nls.append(l)
    return nls;

nLines = lines.copy()
pos = 0
while (len(nLines)>1):
    nr = countOnes(nLines, pos)
    if nr>=len(nLines)/2:
        nLines2 = filterLines(nLines, pos, '1')
    else:
        nLines2 = filterLines(nLines, pos, '0')
    pos+= 1
    nLines = nLines2.copy()
    print (nLines)

oxy = int(nLines[0], 2)
print (nLines, oxy)

nLines = lines.copy()
pos = 0
while (len(nLines)>1):
    nr = countOnes(nLines, pos)
    if nr>=len(nLines)/2:
        nLines2 = filterLines(nLines, pos, '0')
    else:
        nLines2 = filterLines(nLines, pos, '1')
    pos+= 1
    nLines = nLines2.copy()
    print (nLines)

co2 = int(nLines[0], 2)
print (nLines, co2)

print (oxy*co2)
