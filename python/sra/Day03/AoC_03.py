# -*- coding: utf-8 -*-

text = open("input_test.txt").read()
text = open("input.txt").read()

lines = text.split('\n')
while lines[-1] == "":
    lines = lines[:-1]

bitcount = []
for c in lines[0]:
    bitcount.append(int(c))

for l in range(len(lines)-1):
    for i in range(len(lines[0])):
        bitcount[i] += int(lines[l+1][i])

gamma = ''
epsilon = ''
for c in bitcount:
    if c > len(lines)/2:
        gamma += '1'
        epsilon += '0'
    else:
        gamma += '0'
        epsilon += '1'

print (bitcount, len(lines))
print(epsilon, gamma)
print (int(epsilon, 2), int(gamma, 2))
print (int(epsilon, 2)* int(gamma, 2))
