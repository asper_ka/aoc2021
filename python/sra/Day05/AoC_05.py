# -*- coding: utf-8 -*-
import re

text = open("input_test.txt").read()
text = open("input.txt").read()

lines = text.split('\n')
while lines[-1] == "":
    lines = lines[:-1]

def getCoords(x1, x2):
    d = int((x2-x1)/abs(x2-x1))
    return list(range(x1, x2+d, d))

grid = {}
for l in lines:
    m = re.search("(\d*),(\d*) -> (\d*),(\d*)", l)
    x1 = int(m.group(1))
    y1 = int(m.group(2))
    x2 = int(m.group(3))
    y2 = int(m.group(4))
    if (x1==x2):
        for y in getCoords(y1,y2):
            if not (x1,y) in grid:
                grid[(x1,y)] = 1
            else:
                grid[(x1, y)] += 1
    elif (y1==y2):
        for x in getCoords(x1,x2):
            if not (x, y1) in grid:
                grid[(x,y1)] = 1
            else:
                grid[(x,y1)] += 1

nr=0
for c in grid:
    if grid[c] >= 2:
        nr += 1

print (nr)

