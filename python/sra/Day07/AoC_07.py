# -*- coding: utf-8 -*-
import re

text = "16,1,2,0,4,2,7,1,2,14"
text = open("input.txt").read()

nrs_text = text.split(',')
nrs = [int(x) for x in nrs_text]
print (nrs, min(nrs), max(nrs))

minpos = 0
minfuel = len(nrs)*abs(max(nrs)-min(nrs))

for p in range (min(nrs)+1, max(nrs)):
    f = 0
    for n in nrs:
        f += abs(n-p)
    if f < minfuel:
        minpos = p
        minfuel = f

print (minpos, minfuel)