# -*- coding: utf-8 -*-

text = open("input_test.txt").read()
#text = open("input.txt").read()

lines = text.split('\n')
while lines[-1] == "":
    lines = lines[:-1]

d = 0
pos = 0
for l in lines:
    tkns = l.split()
    cmd = tkns[0]
    value = int(tkns[1])
    if (cmd == 'forward'):
        pos += value
    if (cmd == 'up'):
        d -= value
    if (cmd == 'down'):
        d += value

print (pos*d)


