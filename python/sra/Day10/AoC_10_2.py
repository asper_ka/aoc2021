# -*- coding: utf-8 -*-
import re

text = open("input_test.txt").read()
text = open("input.txt").read()

lines = text.split('\n')
while lines[-1] == "":
    lines = lines[:-1]

scores = []
for l in lines:
    #print (l)
    score = 0
    stack = []
    corrupted = False
    for c in l:
        #print (c, stack)
        if c == '(':
            stack.append(')')
        elif c == '[':
            stack.append(']')
        elif c == '{':
            stack.append('}')
        elif c == '<':
            stack.append('>')
        else:
            expected = stack.pop()
            if expected != c:
                corrupted = True
                break
    if not corrupted:
        score = 0
        for c in reversed(stack):
            score *= 5
            if c == ')':
                score += 1
            elif c == ']':
                score += 2
            elif c == '}':
                score += 3
            elif c == '>':
                score += 4
        scores.append(score)

print (sorted(scores)[int(len(scores)/2)])