# -*- coding: utf-8 -*-
import re

text = open("input_test.txt").read()
text = open("input.txt").read()

lines = text.split('\n')
while lines[-1] == "":
    lines = lines[:-1]

score = 0
for l in lines:
    #print (l)
    stack = []
    for c in l:
        #print (c, stack)
        if c == '(':
            stack.append(')')
        elif c == '[':
            stack.append(']')
        elif c == '{':
            stack.append('}')
        elif c == '<':
            stack.append('>')
        else:
            expected = stack.pop()
            if expected != c:
                print ("Syntax Error: '{2}': expected {0}, got {1}".format(expected, c, l))
                if c == ')':
                    score += 3
                elif c == ']':
                    score += 57
                elif c == '}':
                    score += 1197
                elif c == '>':
                    score += 25137
                break

print (score)