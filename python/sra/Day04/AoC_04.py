# -*- coding: utf-8 -*-

text = open("input_test.txt").read()
text = open("input.txt").read()

lines = text.split('\n')
while lines[-1] == "":
    lines = lines[:-1]

numbers = lines[0].split(',')

boards = []
marks = []
l = 2

while l < len(lines):
    marks.append([0]*25)
    nrs = []
    for ll in range(5):
        nrs.extend(lines[l+ll].split())
    boards.append(nrs)
    l += 6

print(marks)
print(boards)
print(numbers)

def mark_number(m, b, nr):
    for i in range (len(b)):
        if b[i] == nr:
            m[i] = 1
    #print (nr)
    #print (m)
    #print (b)
    #print('')

def check_rows_cols(m):
    for i in range (5):
        if (m[i*5] + m[i*5 + 1] + m[i*5 + 2] + m[i*5+ 3]+ m[i*5+ 4])==5:
            print ('row {}'.format(i))
            return True
        if (m[i] + m[i+5] + m[i+10] + m[i+15]+ m[i+20])==5:
            print ('col {}'.format(i))
            return True
    return False

def get_score(b,m):
    sum = 0;
    for i in range (25):
        if (m[i]==0):
            sum += int(b[i])
    return sum

winner = False
for nr in numbers:
    if not winner:
        print(nr)
        for i in range (len(boards)):
            mark_number(marks[i], boards[i], nr)
            if check_rows_cols(marks[i]):
                print ("{0} wins".format(i))
                print (marks[i])
                print (boards[i])
                print (get_score(boards[i], marks[i]))
                print (get_score(boards[i], marks[i])*int(nr))
                winner = True
