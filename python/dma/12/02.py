#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()

class Node:
    Small = 1
    Large = 2
    Start = 3
    End = 4
    def __init__(self, name):
        self.name = name
        
        if name == 'start':
            self.type = self.Start
        elif name == 'end':
            self.type = self.End
        elif name == name.lower():
            self.type = self.Small
        else:
            self.type = self.Large
        self.neighbors = set()

    def __repr__(self) -> str:
        return f'{self.name} ({self.type})'

    
graph = {}
start_node = None
end_node = None

with open(f'{script_dir}/input') as input:
    for line in input.readlines():
        name1, name2 = line.strip().split('-')
        
        if name1 not in graph:
            graph[name1] = Node(name1)
        if name2 not in graph:
            graph[name2] = Node(name2)
        n1 = graph[name1]
        n2 = graph[name2]
        graph[name1].neighbors.add(n2)
        graph[name2].neighbors.add(n1)
        for name in (name1, name2):
            if name == 'start':
                start_node = graph[name]
            elif name == 'end':
                end_node = graph[name]


class PathCount():
    def __init__(self) -> None:
        self.count = 0

    def count_paths(self, start:Node, end:Node, visited):
        if start is end:
            self.count += 1
            return
        if start.type != Node.Large:
            visited.append(start)

        for n in start.neighbors:
            if n in visited:
                if n.type != Node.Small:
                    continue
                elif self.has_any_duplicate_small(visited):
                    continue
            self.count_paths(n, end, visited[:])
                            
    def has_any_duplicate_small(self, visited):
        smalls = [v for v in visited if v.type == Node.Small]
        return len(smalls) > len(set(smalls))

c = PathCount()
visited = []
c.count_paths(start_node, end_node, visited)
print(c.count)
