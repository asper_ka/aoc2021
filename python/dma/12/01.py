#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()


graph = {}

class Node:
    def __init__(self, name):
        self.name = name
        self.is_small = name == name.lower()
        self.neighbors = set()
    

start_node = None
end_node = None

with open(f'{script_dir}/sample_input1') as input:
    for line in input.readlines():
        name1, name2 = line.strip().split('-')
        
        if name1 not in graph:
            graph[name1] = Node(name1)
        if name2 not in graph:
            graph[name2] = Node(name2)
        n1 = graph[name1]
        n2 = graph[name2]
        graph[name1].neighbors.add(n2)
        graph[name2].neighbors.add(n1)
        for name in (name1, name2):
            if name == 'start':
                start_node = graph[name]
            elif name == 'end':
                end_node = graph[name]


class PathCount():
    def __init__(self) -> None:
        self.count = 0

    def count_paths(self, start:Node, end:Node, visited):
        if start.is_small:
            visited.append(start)
        if start is end:
            self.count += 1
        else:
            for n in start.neighbors:
                if n in visited:
                    continue
                self.count_paths(n, end, visited[:])

c = PathCount()
visited = []
c.count_paths(start_node, end_node, visited)
print(c.count)
