#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()

lines = []
with open(f'{script_dir}/input') as f:
    for line in f.readlines():
        line = line.strip()
        if not line:
            continue
        lines.append(line)

opening = '([{<'
closing = {
    '(': ')',
    '[': ']',
    '{': '}',
    '<': '>'
}
error_scores = {
    ')': 3,
    ']': 57,
    '}': 1197,
    '>': 25137
}

def get_error_score(line):
    stack = []
    for c in line:
        if c in opening:
            stack.append(c)
        elif c in closing.values():
            if c != closing[stack.pop()]:
                return error_scores[c]
        else:
            raise ValueError
    return 0

error_sum = sum(map(get_error_score, lines))
print(error_sum)
