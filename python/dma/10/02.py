#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()

lines = []
with open(f'{script_dir}/input') as f:
    for line in f.readlines():
        line = line.strip()
        if not line:
            continue
        lines.append(line)

opening = '([{<'
closing = {
    '(': ')',
    '[': ']',
    '{': '}',
    '<': '>'
}
completion_scores = {
    ')': 1,
    ']': 2,
    '}': 3,
    '>': 4
}

def is_corrupted(line):
    stack = []
    for c in line:
        if c in opening:
            stack.append(c)
        elif c in closing.values():
            if c != closing[stack.pop()]:
                return True
        else:
            raise ValueError
    return False

def get_completed_line_score(line):
    score = 0
    stack = []
    for c in line:
        if c in opening:
            stack.append(c)
        elif c in closing.values():
            if c != closing[stack.pop()]:
                raise Exception('line is corrupted')
        else:
            raise ValueError
    while len(stack) > 0:
        cc = closing[stack.pop()]
        line += cc
        score *= 5
        score += completion_scores[cc]
    return score

lines = filter(lambda l: not is_corrupted(l), lines)
scores = [get_completed_line_score(line) for line in lines]
middle = sorted(scores)[int(len(scores)/2)]
print(middle)
