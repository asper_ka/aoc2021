#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()

nums = []
for line in open(f'{script_dir}/input'):
	nums.append(int(line))

partial_sums = []
for start in range(len(nums)-2):
	cur_sum = sum(nums[start:start+3])
	partial_sums.append(cur_sum)


last = None
increased = 0
for psum in partial_sums:
	if last is not None and psum > last:
		increased += 1
	last = psum

print(f'Solution part 2: {increased}')
