#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()

last = None
increased = 0
for line in open(f'{script_dir}/input'):
	num = int(line)
	if last is not None and num > last:
		increased += 1
	last = num

print(f'Solution part 1: {increased}')
