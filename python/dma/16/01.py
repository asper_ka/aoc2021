#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pathlib


script_dir = pathlib.Path(__file__).parent.resolve()


class Decoder:

    LITERAL_VALUE = 4

    def __init__(self, data) -> None:
        self.num_bits_read = 0
        self.data = ''
        self.sum_of_versions = 0
        self.cur_pos = 0
        for c in data:
            if c.isalpha():
                binary = f'{ord(c)-55:04b}'
            elif c.isdigit():
                binary = f'{int(c):04b}'
            for b in binary:
                self.data += b

    def get_next(self, num):
        result = self.data[self.cur_pos:self.cur_pos+num]
        self.cur_pos += num
        return result

    def get_next_as_int(self, num):
        part = self.get_next(num)
        return int(part, 2)

    def get_version(self):
        version = self.get_next_as_int(3)
        return version

    def get_typeid(self):
        type_id = self.get_next_as_int(3)
        return type_id

    def get_literal_value(self):
        number = ''
        while True:
            data = self.get_next(5)
            is_last = bool(data[0]=='0')
            number += data[1:]
            if is_last:
                value = int(number, 2)
                return

    def read_packet(self):
        self.num_bits_read = 0
        version = self.get_version()
        self.sum_of_versions += version
        type_id = self.get_typeid()
        if type_id == self.LITERAL_VALUE:
            self.get_literal_value()
        else:
            self.read_operator_packet()

    def read_operator_packet(self):
        type_id = self.get_next_as_int(1)
        if type_id == 0:
            length = self.get_next_as_int(15)
            total_read = 0
            while total_read < length:
                self.read_packet()
                total_read += self.num_bits_read
        elif type_id == 1:
            num_packets = self.get_next_as_int(11)
            for _ in range(num_packets):
                self.read_packet()

    def run(self):
        self.read_packet()

    def get_sum_of_versions(self):
        try:
            self.run()
        except ValueError:
            print('parsing error occurred')
        return self.sum_of_versions

def read_file():
    with open(f'{script_dir}/input') as input:
        return input.read().strip()

#data = 'D2FE28'
#data = '38006F45291200'
#data = 'EE00D40C823060'
#data = '8A004A801A8002F478'
#data = '620080001611562C8802118E34'
#data = 'C0015000016115A2E0802F182340'
#data = 'A0016C880162017C3686B18A3D4780'
data = read_file()

print(Decoder(data).get_sum_of_versions())
