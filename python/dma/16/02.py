#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pathlib

script_dir = pathlib.Path(__file__).parent.resolve()

class ValueNode:
    def __init__(self, value) -> None:
        self.value = value

    @property
    def eval(self):
        return self.value

class OperatorNode:
    def __init__(self, type_id) -> None:
        self.type_id = type_id
        self.nodes = []

    @property
    def eval(self):
        if self.type_id == 0:
            return np.sum([n.eval for n in self.nodes])    
        elif self.type_id == 1:
            return np.product([n.eval for n in self.nodes])    
        elif self.type_id == 2:
            return np.min([n.eval for n in self.nodes])    
        elif self.type_id == 3:
            return np.max([n.eval for n in self.nodes])    
        elif self.type_id == 5:
            assert len(self.nodes) == 2
            a, b = self.nodes
            return 1 if a.eval > b.eval else 0
        elif self.type_id == 6:
            assert len(self.nodes) == 2
            a, b = self.nodes
            return 1 if a.eval < b.eval else 0
        elif self.type_id == 7:
            assert len(self.nodes) == 2
            a, b = self.nodes
            return 1 if a.eval == b.eval else 0


class Decoder:
    def __init__(self, data) -> None:
        self.cur_pos = 0
        self.data = ''
        for c in data:
            if c.isalpha():
                binary = f'{ord(c)-55:04b}'
            elif c.isdigit():
                binary = f'{int(c):04b}'
            for b in binary:
                self.data += b

    def get_next(self, num):
        result = self.data[self.cur_pos:self.cur_pos+num]
        self.cur_pos += num
        return result

    def get_next_as_int(self, num):
        part = self.get_next(num)
        return int(part, 2)

    def get_version(self):
        version = self.get_next_as_int(3)
        return 3, version

    def get_typeid(self):
        type_id = self.get_next_as_int(3)
        return 3, type_id

    def get_literal_value(self):
        number = ''
        num_bits = 0
        while True:
            data = self.get_next(5)
            num_bits += 5
            is_last = bool(data[0]=='0')
            number += data[1:]
            if is_last:
                value = int(number, 2)
                return num_bits, ValueNode(value)

    def read_packet(self):
        total_bits = 0

        num_bits, _ = self.get_version()
        total_bits += num_bits

        num_bits, type_id = self.get_typeid()
        total_bits += num_bits

        if type_id == 4:
            num_bits, node = self.get_literal_value()
            total_bits += num_bits
        else:
            node = OperatorNode(type_id)
            sub_nodes, num_bits = self.read_operator_packet()
            node.nodes = sub_nodes
            total_bits += num_bits
        return node, total_bits

    def read_operator_packet(self):
        total_bits = 0
        type_id = self.get_next_as_int(1)
        total_bits += 1
        sub_nodes = []
        if type_id == 0:
            length = self.get_next_as_int(15)
            total_bits += 15
            total_packet_bits = 0
            while total_packet_bits < length:
                sub_node, num_bits = self.read_packet()
                total_packet_bits += num_bits
                sub_nodes.append(sub_node)
            total_bits += total_packet_bits
        elif type_id == 1:
            num_packets = self.get_next_as_int(11)
            total_bits += 11
            for _ in range(num_packets):
                sub_node, num_bits = self.read_packet()
                total_bits += num_bits
                sub_nodes.append(sub_node)
        return sub_nodes, total_bits

    def get_root_node(self):
        node, _ = self.read_packet()
        return node


def read_file():
    with open(f'{script_dir}/input') as input:
        return input.read().strip()

# part 2 examples
#data = 'C200B40A82'
#data = '04005AC33890'
#data = '880086C3E88112'
#data = 'CE00C43D881120'
#data = 'D8005AC2A8F0'
#data = 'F600BC2D8F'
#data = '9C005AC2F8F0'
#data = '9C0141080250320F1802104A08'

data = read_file()

node = Decoder(data).get_root_node()
print(node.eval)
