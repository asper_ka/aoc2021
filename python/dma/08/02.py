#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()

code_lines = []
display_lines = []
with open(f'{script_dir}/input') as f:
    for line in f.readlines():
        code, display = line.split('|')
        code_lines.append([set(c) for c in code.strip().split(' ')])
        display_lines.append(''.join(sorted(d)) for d in display.strip().split(' '))


def decode_line(codes, display):
    tr = {}
    assert(len(codes) == 10)
    remaining = codes[:]
    for code in codes:
        if len(code) == 2:
            tr[1] = code
            remaining.remove(code)
        elif len(code) == 4:
            tr[4] = code
            remaining.remove(code)
        elif len(code) == 3:
            tr[7] = code
            remaining.remove(code)
        elif len(code) == 7:
            tr[8] = code
            remaining.remove(code)
    
    # found 1,4,7,8
    # missing 0,2,3,5,6,9

    codes = remaining
    assert(len(codes) == 6)
    remaining = codes[:]
    for code in codes:
        if tr[4].issubset(code):
            if len(code) == 6:
                tr[9] = code
                remaining.remove(code)

    # found 1,4,7,8,9
    # missing 0,2,3,5,6

    codes = remaining
    assert(len(codes) == 5)
    remaining = codes[:]
    for code in codes:
        if tr[7].issubset(code):
            if len(code) == 5:
                tr[3] = code
                remaining.remove(code)
            elif len(code) == 6:
                tr[0] = code
                remaining.remove(code)

    # found 0,1,3,4,7,8,9
    # missing 2,5,6

    codes = remaining
    assert(len(codes) == 3)
    remaining = codes[:]
    for code in codes:
        if len(code) == 6:
            tr[6] = code
            remaining.remove(code)

    # found 0,1,3,4,6,7,8,9
    # missing 2,5

    codes = remaining
    assert(len(codes) == 2)
    remaining = codes[:]
    for code in codes:
        if code.issubset(tr[9]):
            tr[5] = code
            remaining.remove(code)

    codes = remaining
    assert(len(codes) == 1)
    tr[2] = codes[0]

    tr = {''.join(sorted(list(v))): k for k, v in tr.items()}
    number = ''
    for digit in display:
        number += str(tr[digit])
    number = int(number)
    return number


def main():
    result = 0

    for idx in range(len(code_lines)):
        codes = code_lines[idx]
        display = display_lines[idx]
        number = decode_line(codes, display)
        result += number

    print(result)


main()
