#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()

codes = []
displays = []
with open(f'{script_dir}/input') as f:
    for line in f.readlines():
        code, display = line.split('|')
        codes.append(code.strip().split(' '))
        displays.append(display.strip().split(' '))

lengths = {1: 2, 4: 4, 7:3, 8:7}

num_digits = 0
for display in displays:
    for digit in display:
        if len(digit) in lengths.values():
            num_digits += 1

print(num_digits)
