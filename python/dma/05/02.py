#!/usr/bin/env python
# -*- coding: utf-8 -*-

import collections
import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()

coords = []
for line in open(f'{script_dir}/input'):
    start, stop = line.split('->')
    x1, y1 = start.split(',')
    x2, y2 = stop.split(',')
    x1 = int(x1)
    y1 = int(y1)
    x2 = int(x2)
    y2 = int(y2)
    coords.append([x1, y1, x2, y2])

field = collections.defaultdict(int)


for x1, y1, x2, y2 in coords:
    if x2 > x1:
        xs = range(x1, x2+1)
    else:
        xs = range(x1, x2-1, -1)

    if y2 > y1:
        ys = range(y1, y2+1)
    else:
        ys = range(y1, y2-1, -1)

    if x1 == x2:
        for y in ys:
            field[(x1, y)] += 1
    elif y1 == y2:
        for x in xs:
            field[(x, y1)] += 1
    else:
        for x, y in zip(xs, ys):
            field[(x, y)] += 1


counter = collections.Counter(field.values())
del counter[1]
print(f'result: {sum(counter.values())}')
