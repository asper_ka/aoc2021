#!/usr/bin/env python
# -*- coding: utf-8 -*-

import collections
import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()


def read_from_file():
    with open(f'{script_dir}/input') as input:
        template = input.readline().strip()
        rules = {}
        for line in input.readlines():
            a, b = line.split('->')
            rules[a.strip()] = b.strip()
    return template, rules


template, rules = read_from_file()

pairs = collections.defaultdict(int)
for start in range(len(template)-1):
    pair = template[start:start+2]
    pairs[pair] += 1

letters = collections.defaultdict(int)
for c in template:
    letters[c] += 1

num_steps = 40
for _ in range(num_steps):
    new_pairs = collections.defaultdict(int)
    for pair, num in pairs.items():
        if pair in rules:
            insertion = rules[pair]
            n1 = pair[0] + insertion
            new_pairs[n1] += num
            n2 = insertion + pair[1]
            new_pairs[n2] += num
            letters[insertion] += num

    pairs = new_pairs

least_common = min(letters, key=letters.get)
most_common = max(letters, key=letters.get)

print('least common', least_common, 'most common', most_common)
print('difference', letters[most_common]-letters[least_common])

