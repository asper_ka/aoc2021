#!/usr/bin/env python
# -*- coding: utf-8 -*-

import collections
import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()

with open(f'{script_dir}/input') as input:
    template = [c for c in input.readline().strip()]
    rules = {}
    for line in input.readlines():
        a, b = line.split('->')
        rules[a.strip()] = b.strip()

for _ in range(10):
    insertions = {}
    for pattern in rules.keys():
        for idx in range(len(template)-1):
            if template[idx] == pattern[0] and template[idx+1] == pattern[1]:
                insertions[idx] = rules[pattern]

    delta = 0

    for position in sorted(insertions.keys()):
        insertion = insertions[position]
        template.insert(position+1+delta, insertion)
        delta += 1

occurrences = collections.Counter(template).values()
result = max(occurrences) - min(occurrences)
print(result)