#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()

field = []
with open(f'{script_dir}/input') as input:
    for line in input.readlines():
        field.append([int(c) for c in line.strip()])


field = np.array(field, dtype=int)
num_rows, num_cols = field.shape

def get_neighbors(r, c):
    n = []
    if r>0:
        n.append([r-1, c])
        if c>0:
            n.append([r-1, c-1])
        if c<num_cols-1:
            n.append([r-1, c+1])

    if c>0:
        n.append([r, c-1])
    if c<num_cols-1:
        n.append([r, c+1])

    if r<num_rows-1:
        n.append([r+1, c])
        if c>0:
            n.append([r+1, c-1])
        if c<num_cols-1:
            n.append([r+1, c+1])    
    return n


def get_new():
    num_new = 0
    for r in range(num_rows):
        for c in range(num_cols):
            if field[r, c] > 9 and not has_flashed[r, c]:
                has_flashed[r, c] = True
                num_new += 1
                for r, c in get_neighbors(r, c):
                    field[r, c] += 1
    return num_new

num_flashes = 0
for _ in range(100):
    field[field>9] = 0
    has_flashed = np.zeros_like(field, dtype=bool)
    field += 1

    while True:
        num_new = get_new()
        num_flashes += num_new
        if num_new == 0:
            break

print(num_flashes)
