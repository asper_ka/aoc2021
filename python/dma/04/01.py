#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()

lines = []
for line in open(f'{script_dir}/input'):
    line = line.strip()
    if not line:
        continue
    lines.append(line)

numbers = [int(n) for n in lines.pop(0).split(',')]

class Board:
    def __init__(self, rows):
        self.board = np.array(rows)
        self.drawn = np.zeros_like(self.board, dtype=bool)
        self.wins = False

    def _check_win(self):
        for row in range(5):
            if all(self.drawn[row]):
                self.wins = True
                return
        for col in range(5):
            if all(self.drawn[:, col]):
                self.wins = True
                return
    
    def mark(self, num):
        if num in self.board:
            pos = np.where(self.board == num)
            self.drawn[pos] = True
            self._check_win()

    def get_unmarked_sum(self):
        return np.sum(self.board[self.drawn == False])


boards = []
while lines:
    rows = []
    for _ in range(5):
        row = [int(n) for n in lines.pop(0).split()]
        rows.append(row)
    
    boards.append(Board(rows))


def run_game(numbers):
    for num in numbers:
        for i, board in enumerate(boards):
            board.mark(num)
            if board.wins:
                print(f'board {i} wins with number {num}')
                print(f'result {num*board.get_unmarked_sum()}')
                return


run_game(numbers)
