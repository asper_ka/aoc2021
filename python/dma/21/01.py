#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pathlib

script_dir = pathlib.Path(__file__).parent.resolve()

class Dice:
    def __init__(self):
        self.next_value = 1
        self.num_rolls = 0

    def roll(self):
        self.num_rolls += 1
        value = self.next_value
        new_value = value + 1
        if new_value > 100:
            new_value = 1
        self.next_value = new_value
        return value

class Player:
    def __init__(self, pos, dice):
        self.pos = pos
        self.dice = dice
        self.score = 0

    def play(self):
        value_sum = sum([self.dice.roll() for _ in range(3)])
        new_pos = self.pos + value_sum
        while new_pos > 10:
            new_pos -= 10
        self.pos = new_pos
        self.score += new_pos


def run_game(players):
    while True:
        for i, p in enumerate(players):
            p.play()
            if p.score >= 1000:
                print(f'player {i+1} wins')
                losing = players[0] if i==1 else players[1]
                print(f'{losing.score*p.dice.num_rolls}')
                return
                
def main():
    dice = Dice()
    players = [Player(9, dice), Player(10, dice)]
    run_game(players)

main()
