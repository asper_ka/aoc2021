#!/usr/bin/env python
# -*- coding: utf-8 -*-

import collections
import pathlib
import itertools

script_dir = pathlib.Path(__file__).parent.resolve()

dice_sums = [sum(x) for x in itertools.product([1,2,3],repeat=3)]           
games = collections.defaultdict(int)

# games[((4, 0), (8, 0))] = 1
games[((9, 0), (10, 0))] = 1

num_wins = [0, 0]
num_rounds = 0

while True:
    num_rounds += 1
    if not games:
        break
    for player_idx in range(2):
        new_games = collections.defaultdict(int)
        for game, num_games in games.items():
            position, score = game[player_idx]
            idle_player = game[0] if player_idx == 1 else game[1]
            for current_sum in dice_sums:
                new_position = position + current_sum
                while new_position > 10:
                    new_position -= 10
                new_score = score+new_position
                if new_score >= 21:
                    num_wins[player_idx] += num_games
                else:
                    if player_idx == 0:
                        new_games[((new_position, new_score), idle_player)] += num_games
                    else:
                        new_games[(idle_player, (new_position, new_score))] += num_games
        games = new_games

    print(f'rounds: {num_rounds}, wins: {num_wins}')

print(max(num_wins))
