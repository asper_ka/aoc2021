#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pathlib
import collections

LIGHT = '#'
DARK = '.'

script_dir = pathlib.Path(__file__).parent.resolve()

fncode = 'sample_code'
fnimage = 'sample_image'
fncode = 'code'
fnimage = 'image'

with open(f'{script_dir}/{fncode}') as input:
    code = input.read().strip()

with open(f'{script_dir}/{fnimage}') as input:
    image = [l.strip() for l in input.read().split('\n')]

field = collections.defaultdict(lambda: DARK)

for row in range(len(image)):
    for col in range(len(image[row])):
        field[(row, col)] = image[row][col]

def get_elements(r, c):
    result = []
    for dr in (-1, 0, 1):
        for dc in (-1, 0, 1):
            result.append((r+dr, c+dc))
    return result

def get_addr(field, r, c):
    elements = get_elements(r, c)
    addr = ''
    for nr, nc in elements:
        if field[(nr, nc)] == LIGHT:
            addr += '1'
        else:
            addr += '0'
    return int(addr, 2)


num_runs = 50
for i in range(1,num_runs+1):
    print(f'run {i}')
    rmin = 0-i
    rmax = len(image)-1+i
    cmin = 0-i
    cmax = len(image[0])-1+i
    if code[0] == LIGHT:
        if i %2 == 1:
            new_field = collections.defaultdict(lambda: LIGHT)
        else:
            new_field = collections.defaultdict(lambda: DARK)
    else:
        new_field = collections.defaultdict(lambda: DARK)
    for r in range(rmin, rmax+1):
        for c in range(cmin, cmax+1):
            addr = get_addr(field, r, c)
            new_value = code[addr]
            new_field[(r, c)] = new_value

    field = new_field
    
print(collections.Counter(field.values())[LIGHT])

#for r in range(rmin, rmax+1):
#    print(''.join([field[(r, c)] for c in range(cmin, cmax+1)]))
