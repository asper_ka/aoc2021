#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pathlib

script_dir = pathlib.Path(__file__).parent.resolve()

dots = set()
with open(f'{script_dir}/dots') as input:
    for line in input.readlines():
        x, y = line.split(',')
        dots.add((int(x), int(y)))

instructions = []
with open(f'{script_dir}/instructions') as input:
    for line in input.readlines():
        line = line.strip().replace('fold along ', '')
        name, value = line.split('=')
        instructions.append((name, int(value)))

for fold_coord, fold_value in instructions:
    folded = set()
    for dot in dots:
        x, y = dot
        if fold_coord == 'x':
            if x > fold_value:
                x -= 2*(x-fold_value)
            elif x == fold_value:
                raise ValueError('on fold')
        elif fold_coord == 'y':
            if y > fold_value:
                y -= 2*(y-fold_value)
            elif y == fold_value:
                raise ValueError('on fold')
        folded.add((x, y))
    dots = folded

xs = [d[0] for d in dots]
ys = [d[1] for d in dots]

f = np.zeros(shape=(max(ys)+1, max(xs)+1), dtype=int)
for y, x in zip(ys, xs):
    f[y, x] = 1

for row in f:
    print(''.join(['#' if v==1 else ' ' for v in row]))
