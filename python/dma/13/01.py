#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import numpy as np
import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()

dots = set()
with open(f'{script_dir}/sample_dots') as input:
    for line in input.readlines():
        x, y = line.split(',')
        dots.add((int(x), int(y)))

fy = 7

folded = set()
for dot in dots:
    x, y = dot
    if y > fy:
        y -= 2*(y-fy)
    elif y == fy:
        raise ValueError('on fold')
    folded.add((x, y))

print('after folding')
print(len(folded))
