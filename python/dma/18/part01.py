#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pathlib

script_dir = pathlib.Path(__file__).parent.resolve()

class Reader:
    def __init__(self, input):
        input = input.replace(' ', '')
        self.input = input
        self.position = 0

    def read(self):
        c = self.input[self.position]
        if c == '[':
            self.position += 1
            v1 = self.read()
            self.position += 2
            v2 = self.read()
            self.position += 1
            p = Node()
            p.nodes = [v1, v2]
            return p
        elif c.isdigit():
            p = Node.from_value(int(c))
            return p

    @staticmethod
    def get_sum_with_reduction(inputs):
        node = Reader(inputs[0]).read()
        for input in inputs[1:]:
            node2 = Reader(input).read()
            node = node + node2
            node.reduce()
        return node

class Node:
    def __init__(self):
        self.parent = None
        self._value = None
        self._nodes = None

    @staticmethod
    def from_value(value):
        n = Node()
        n.value = value
        return n

    @property
    def is_number(self):
        return self._value is not None

    @property
    def is_pair(self):
        return self._nodes is not None

    @property
    def value(self):
        return self._value
    
    @value.setter
    def value(self, x):
        self._value = x
        self._nodes = None

    @property
    def nodes(self):
        return self._nodes

    @nodes.setter
    def nodes(self, values):
        for node in values:
            node.parent = self
        self._nodes = values
        self._value = None
        
    def __repr__(self) -> str:
        if self.is_pair:
            return f'[{self.nodes[0]},{self.nodes[1]}]'
        else:
            return str(self.value)

    @property
    def flat(self):
        if self.is_number:
            return [self]
        else:
            nodes = []
            for n in self.nodes:
                nodes += n.flat
            return nodes

    @property
    def root(self):
        p = self
        while True:
            if p.parent is None:
                return p
            p = p.parent

    @property
    def depth(self):
        result = 0
        p = self
        while True:
            p = p.parent
            if p is None:
                break
            result += 1
        return result
    
    @property
    def can_explode(self):
        if self.depth < 4:
            return False
        if self.is_pair:
            if all([n.is_number for n in self.nodes]):
                return True
        return False

    def explode(self):
        if self.can_explode:
            self._add_left()
            self._add_right()
            self.value = 0
            return True
        if self.is_pair:
            for node in self.nodes:
                if node.explode():
                    return True
        return False

    def _add_left(self):
        node = self.nodes[0]
        flat = self.root.flat
        idx = flat.index(node)
        if idx > 0:
            flat[idx-1].value += node.value

    def _add_right(self):
        node = self.nodes[1]
        flat = self.root.flat
        idx = flat.index(node)
        if idx < len(flat)-1:
            flat[idx+1].value += node.value

    def split(self):
        if self.is_number:
            if self.value >= 10:
                low = int(self.value/2.0)
                high = self.value - low
                v1 = Node.from_value(low)
                v2 = Node.from_value(high)
                self.nodes = [v1, v2]
                return True
        elif self.is_pair:
            for node in self.nodes:
                if node.split():
                    return True
        return False

    def __add__(self, node):
        result = Node()
        result.nodes = [self, node]
        self.parent = result
        node.parent = result
        return result

    def reduce(self):
        while True:
            if not self.explode() and not self.split():
                return

    @property
    def magnitude(self):
        if self.is_number:
            return self.value
        else:
            return 3*self.nodes[0].magnitude + 2*self.nodes[1].magnitude


def part1():
    with open(f'{script_dir}/input') as input:
        inputs = [l.strip() for l in input.readlines()]
    node = Reader.get_sum_with_reduction(inputs)
    result = node.magnitude
    print(result)



if __name__ == '__main__':
    part1()
