#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from part01 import *

class TestExplodeOnce(unittest.TestCase):

    def test_explode_once(self):
        self._run('[[[[[9,8],1],2],3],4]', '[[[[0,9],2],3],4]')
        self._run('[7,[6,[5,[4,[3,2]]]]]', '[7,[6,[5,[7,0]]]]')
        self._run('[[6,[5,[4,[3,2]]]],1]', '[[6,[5,[7,0]]],3]')
        self._run('[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]', '[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]')
        self._run('[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]', '[[3,[2,[8,0]]],[9,[5,[7,0]]]]')

    def _run(self, input, expected):
        node = Reader(input).read()
        node.explode()
        result = str(node)
        self.assertEqual(expected, result)


class TestAdd(unittest.TestCase):
    def test_add(self):
        node1 = Reader('[[[[4,3],4],4],[7,[[8,4],9]]]').read()
        node2 = Reader('[1,1]').read()
        node = node1 + node2
        result = str(node)
        self.assertEqual('[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]', result)


class TestReduce(unittest.TestCase):
    def test_steps(self):
        node1 = Reader('[[[[4,3],4],4],[7,[[8,4],9]]]').read()
        node2 = Reader('[1,1]').read()
        node = node1 + node2

        node.explode()
        self.assertEqual('[[[[0,7],4],[7,[[8,4],9]]],[1,1]]', str(node))

        node.explode()
        self.assertEqual('[[[[0,7],4],[15,[0,13]]],[1,1]]', str(node))

        node.split()
        self.assertEqual('[[[[0,7],4],[[7,8],[0,13]]],[1,1]]', str(node))

        node.split()
        self.assertEqual('[[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]', str(node))

        node.explode()
        self.assertEqual('[[[[0,7],4],[[7,8],[6,0]]],[8,1]]', str(node))

    def test_reduce(self):
        node = Reader('[[[[0,7],4],[7,[[8,4],9]]],[1,1]]').read()
        node.reduce()
        self.assertEqual('[[[[0,7],4],[[7,8],[6,0]]],[8,1]]', str(node))


class TestFinalSum(unittest.TestCase):
    def test_simple(self):
        inputs = ['[1,1]', '[2,2]', '[3,3]', '[4,4]']
        self._run(inputs, '[[[[1,1],[2,2]],[3,3]],[4,4]]')

    def _run(self, inputs, expected):
        node = Reader(inputs[0]).read()
        for input in inputs[1:]:
            node = node + Reader(input).read()
        result = str(node)
        self.assertEqual(expected, result)


class TestFinalSumWithReduction(unittest.TestCase):

    def test_with_reduction(self):
        inputs = ['[1,1]', '[2,2]', '[3,3]', '[4,4]', '[5,5]']
        self._run(inputs, '[[[[3,0],[5,3]],[4,4]],[5,5]]')

        inputs = ['[1,1]', '[2,2]', '[3,3]', '[4,4]', '[5,5]', '[6,6]']
        self._run(inputs, '[[[[5,0],[7,4]],[5,5]],[6,6]]')

    def test_large_example(self):
        inputs = [
            '[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]',
            '[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]',
            '[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]',
            '[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]',
            '[7,[5,[[3,8],[1,4]]]]',
            '[[2,[2,2]],[8,[8,1]]]',
            '[2,9]',
            '[1,[[[9,3],9],[[9,0],[0,7]]]]',
            '[[[5,[7,4]],7],1]',
            '[[[[4,2],2],6],[8,7]]']
        self._run(inputs, '[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]')

    def _run(self, inputs, expected):
        node = Reader.get_sum_with_reduction(inputs)
        result = str(node)
        self.assertEqual(expected, result)


class TestMagnitude(unittest.TestCase):

    def test_magnitude(self):
        self._run('[[1,2],[[3,4],5]]', 143)
        self._run('[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]', 3488)

    def _run(self, input, expected):
        node = Reader(input).read()
        result = node.magnitude
        self.assertEqual(expected, result)

    def test_large_example(self):
        inputs = [
            '[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]',
            '[[[5,[2,8]],4],[5,[[9,9],0]]]',
            '[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]',
            '[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]',
            '[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]',
            '[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]',
            '[[[[5,4],[7,7]],8],[[8,3],8]]',
            '[[9,3],[[9,9],[6,[4,9]]]]',
            '[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]',
            '[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]'
        ]

        node = Reader.get_sum_with_reduction(inputs)
        result = node.magnitude
        self.assertEqual(4140, result)


class TestFlat(unittest.TestCase):
    def test_flat(self):
        input = '[1,[2,[3,4]]]'
        node = Reader(input).read()
        flat = node.flat
        self.assertEqual('[1, 2, 3, 4]', str(flat))
        

if __name__ == "__main__":
    unittest.main()
