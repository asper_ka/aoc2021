#!/usr/bin/env python
# -*- coding: utf-8 -*-

from part01 import *

def part2():
    with open(f'{script_dir}/input') as input:
        inputs = [l.strip() for l in input.readlines()]
    
    max_magnitude = 0
    for i1 in range(len(inputs)):
        for i2 in range(len(inputs)):
            if i1 == i2:
                continue
            node = Reader.get_sum_with_reduction([inputs[i1], inputs[i2]])
            result = node.magnitude
            if result > max_magnitude:
                max_magnitude = result

    print(max_magnitude)


if __name__ == '__main__':
    part2()
