#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pathlib
import re

script_dir = pathlib.Path(__file__).parent.resolve()

with open(f'{script_dir}/input') as input:
    line = input.read().strip()
    m = re.search('x=([-\d]+)\.\.([-\d]+), y=([-\d]+)\.\.([-\d]+)', line)
    xmin, xmax, ymin, ymax = [int(v) for v in m.groups()]


def get_highest_y(vx, vy):
    x, y = 0, 0
    highest_y = 0
    while True:
        if xmin <= x <= xmax and ymin <= y <= ymax:
            #print(f'reaches target at x={x}, y={y}')
            #print(f'highest y {highest_y}')
            return highest_y
        elif x > xmax or y < ymin:
            #print(f'passed target at x={x}, y={y}')
            raise ValueError
        x += vx
        y += vy
        if y > highest_y:
            highest_y = y
        if vx > 0:
            vx -= 1
        elif vx < 0:
            vx += 1
        vy -= 1


max_peak = 0
peak_v = None

for vx in range(1000):
    for vy in range(1000):
        try:
            peak = get_highest_y(vx, vy)
            if peak > max_peak:
                max_peak = peak
                peak_v = vx, vy
        except ValueError:
            pass

print(max_peak, peak_v)
