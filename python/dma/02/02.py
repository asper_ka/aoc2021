#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()

pos = 0
depth = 0
aim = 0

for line in open(f'{script_dir}/input'):
    command, value_str = line.split()
    value = int(value_str)
    if command == 'forward':
        pos += value
        depth += aim*value
    elif command == 'down':
        aim += value
    elif command == 'up':
        aim -= value

print(f'Solution: {depth * pos}')
