#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()

with open(f'{script_dir}/sample_input') as f:
    fish = [int(n) for n in f.read().split(',')]

print(f'initial: {fish}')

for num in range(80):
    for i in range(len(fish)):
        if fish[i] == 0:
            fish[i] = 6
            fish.append(8)
        else:
            fish[i] -= 1

    print(f'after {num+1} days there are {len(fish)} fish')
