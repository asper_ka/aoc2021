#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()

fish = [0]*9
with open(f'{script_dir}/input') as f:
    data = [int(n) for n in f.read().split(',')]

for d in data:
    fish[d] += 1

for num in range(256):
    new_fish = fish[0]
    fish[:-1] = fish[1:]
    fish[6] += new_fish
    fish[8] = new_fish
    print(f'after {num+1} days there are {sum(fish)} fish')
