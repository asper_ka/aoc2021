#!/usr/bin/env python
# -*- coding: utf-8 -*-

import collections
import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()

numbers = []
for line in open(f'{script_dir}/input'):
    numbers.append(line.strip())

num_len = len(numbers[0])
assert(all([len(n) == num_len for n in numbers]))

gamma = ''
epsilon = ''

for idx in range(num_len):
    c = collections.Counter([n[idx] for n in numbers])
    if c['0'] > c['1']:
        gamma += '0'
        epsilon += '1'
    else:
        gamma += '1'
        epsilon += '0'

gamma = int(gamma,2)
epsilon = int(epsilon,2)

print(f'result {gamma*epsilon}')

