#!/usr/bin/env python
# -*- coding: utf-8 -*-

import collections
import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()

numbers = []
for line in open(f'{script_dir}/input'):
    numbers.append(line.strip())

num_len = len(numbers[0])
assert(all([len(n) == num_len for n in numbers]))


def get_most_common_bit(numbers, pos):
    c = collections.Counter([n[pos] for n in numbers])
    if c['0'] > c['1']:
        return '0'
    else:
        return '1'


def get_least_common_bit(numbers, pos):
    c = collections.Counter([n[pos] for n in numbers])
    if c['0'] > c['1']:
        return '1'
    else:
        return '0'


def get_rating(numbers, func):
    for pos in range(num_len):
        b = func(numbers, pos)
        numbers = [n for n in numbers if n[pos]==b]
        if len(numbers) == 1:
            return int(numbers[0],2)



oxygen = get_rating(numbers[:], get_most_common_bit)
co2 = get_rating(numbers[:], get_least_common_bit)

print(f'result {oxygen*co2}')

