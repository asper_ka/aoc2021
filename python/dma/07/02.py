#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()

with open(f'{script_dir}/input') as f:
    positions = [int(p) for p in f.read().split(',')]


results = []

for target in range(min(positions), max(positions)+1):
    total_fuel = 0
    for pos in positions:
        dist = abs(pos-target)
        fuel = int(dist*(dist+1)/2)
        total_fuel += fuel
    results.append([target, total_fuel])

results = np.array(results).T
min_target_idx = np.argmin(results[1])

print(results[1][min_target_idx])
