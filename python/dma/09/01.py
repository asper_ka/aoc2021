#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()

field = []
with open(f'{script_dir}/input') as f:
    for line in f.readlines():
        field.append([int(c) for c in line.strip()])

field = np.array(field)
num_rows, num_cols = field.shape


def get_neighbors(f, r, c):
    n = []
    if c>0:
        n.append(f[r, c-1])
    if c<num_cols-1:
        n.append(f[r, c+1])
    if r>0:
        n.append(f[r-1, c])
    if r<num_rows-1:
        n.append(f[r+1, c])
    
    return n

def is_low_point(f, r, c):
    value = f[r, c]
    neighbors = get_neighbors(f, r, c)
    return all([value < n for n in neighbors])


result = 0

for r in range(num_rows):
    for c in range(num_cols):
        if is_low_point(field, r, c):
            result += field[r, c]+1

print(result)