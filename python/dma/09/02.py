#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pathlib
script_dir = pathlib.Path(__file__).parent.resolve()

field = []
with open(f'{script_dir}/input') as f:
    for line in f.readlines():
        field.append([int(c) for c in line.strip()])

field = np.array(field)
num_rows, num_cols = field.shape


def get_neighbors(f, r, c):
    n = []
    if c>0:
        n.append([r, c-1])
    if c<num_cols-1:
        n.append([r, c+1])
    if r>0:
        n.append([r-1, c])
    if r<num_rows-1:
        n.append([r+1, c])
    return n

def is_low_point(f, r, c):
    value = f[r, c]
    neighbors = get_neighbors(f, r, c)
    return all([value < f[r,c] for r,c in neighbors])


def get_basin_points(f, fs, r, c):
    p = set()
    if (r,c) in fs:
        p.add((r, c))
        fs.remove((r,c))
    neighbors = get_neighbors(f, r, c)
    for r, c in neighbors:
        if (r,c) in fs and f[r,c] < 9:
            np = get_basin_points(f, fs, r, c)
            p = p.union(np)
    return p
        

def get_field_set(f):
    field_set = set()
    for r in range(num_rows):
        for c in range(num_cols):
            field_set.add((r, c))
    return field_set


def main():
    sizes = []
    for r in range(num_rows):
        for c in range(num_cols):
            if is_low_point(field, r, c):
                field_set = get_field_set(field)
                size = len(get_basin_points(field, field_set, r, c))
                sizes.append(size)
    result = np.product(sorted(sizes)[-3:])
    print(result)

main()
