#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import heapq
import pathlib

script_dir = pathlib.Path(__file__).parent.resolve()

field = []
with open(f'{script_dir}/input') as input:
    for line in input.readlines():
        row = [int(c) for c in line.strip()]
        field.append(row)

def get_neighbors(pos):
    r, c = pos
    n = []
    if c>0:
        n.append((r, c-1))
    if c<num_cols-1:
        n.append((r, c+1))
    if r>0:
        n.append((r-1, c))
    if r<num_rows-1:
        n.append((r+1, c))
    
    return n


field = np.array(field)
num_rows, num_cols = field.shape

full = np.tile(field, (5,5))

for row in range(5):
    for col in range(5):
        inc = row+col
        full[row*num_rows:(row+1)*num_rows,col*num_cols:(col+1)*num_cols] += inc

while np.any(full > 9):
    full[full > 9] -= 9

field = full
num_rows, num_cols = field.shape

start = (0, 0)
target = (num_rows-1, num_cols-1)

pq = []
heapq.heappush(pq, (0, start))
risks = {start: 0}
visited = set()

# Find shortest path using A* algorithm
while True:
    priority, current = heapq.heappop(pq)
    risk = risks[current]
    if current == target:
        break
    visited.add(current)

    for n in get_neighbors(current):
        if n not in visited:
            dist = abs(current[0]-target[0]) + abs(current[1]-target[1])
            new_risk = risk + field[n] + dist
            if n in risks and new_risk >= risks[n]:
                continue
            new_prio = field[n] + risk
            heapq.heappush(pq, (new_prio, n))
            risks[n] = field[n] + risk


print('risk', risk)
