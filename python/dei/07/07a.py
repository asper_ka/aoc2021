# -*- coding: utf-8 -*-
"""
Created on Tue Dec  7 05:59:16 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np

fname = 'example.txt'
fname = 'input.txt'

data = pd.read_csv(
    fname, 
    sep = ',', 
    header=None
    )

hpos = data.to_numpy()[0]
hmed = int(np.median(hpos))

# for i in range(-15, 15, 1):
#     print("{:f}: {:f}".format(0.1*i+hmed, abs(hpos - (hmed+0.1*i)).sum()))

result = abs(hpos - hmed).sum()

print('Puzzle solution: {:d}'.format(result))