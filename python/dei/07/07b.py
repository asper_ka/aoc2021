# -*- coding: utf-8 -*-
"""
Created on Tue Dec  7 05:59:16 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np

fname = 'example.txt'
fname = 'input.txt'

data = pd.read_csv(
    fname, 
    sep = ',', 
    header=None
    )

hpos = data.to_numpy()[0]
hmed = int(np.median(hpos))
hmean = int(np.round(np.mean(hpos)))

hof = abs(hpos - (hmean))
fuel0 = (hof * (hof+1) // 2).sum()

minfuel = fuel0

for i in range(-5, 5, 1):
    hof = abs(hpos - (hmean+i))
    fuel = (hof * (hof+1) // 2).sum()
    print("{:d}: {:d}".format(i, fuel-fuel0))
    
    if fuel < minfuel:
        minfuel = fuel

result = minfuel

print('Puzzle solution: {:d}'.format(result))