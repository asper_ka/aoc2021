# -*- coding: utf-8 -*-
"""
Created on Tue Dec  7 05:59:16 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np

#fname = 'example.txt'
fname = 'input.txt'

data = pd.read_csv(
    fname, 
    sep = ',', 
    header=None
    )

hpos = data.to_numpy()[0]
hmed = int(np.median(hpos))
fres = abs(hpos - hmed).sum()

for i in range(-5, 5, 1):
    fnew = abs(hpos - (hmed+i)).sum()
    print("{:d}: {:d} ({:d})".format(i+hmed, fnew, fnew - fres))
    if fnew < fres:
        fres = fnew

result = fres

print('Puzzle solution: {:d}'.format(result))