# -*- coding: utf-8 -*-
"""
Created on Tue Dec 21 21:45:50 2021

@author: PII_DEI
"""
import pandas as pd
import numpy as np
import scipy.signal as ssig

fname = 'example.txt'
fname = 'input.txt'

with open(fname) as f:
    lines = f.read().splitlines()


pos = np.array([int(l.split(':')[1]) for l in lines])

points = np.array([0,0], dtype=np.int64)

dice_rolls = 0
dice_curval = 0

def roll_dice():
    global dice_rolls, dice_curval
    dice_rolls += 1
    dice_curval += 1
    dice_curval = 1 + ((dice_curval-1) % 100)
    # print(dice_curval)


nplayer = 2
pid = 0
while points.max() < 1000:
    for i in range(3):
        roll_dice()
        pos[pid] = 1 + ((pos[pid] + dice_curval - 1)%10)
    points[pid] += pos[pid]
    # print(pid+1, pos[pid], points[pid])
    pid = (pid+1) % nplayer
    

result = points.min()*dice_rolls

print('Puzzle solution: {:d}'.format(result))