# -*- coding: utf-8 -*-
"""
Created on Tue Dec 21 21:45:50 2021

@author: PII_DEI
"""
import numpy as np

fname = 'example.txt'
fname = 'input.txt'

# read positions
with open(fname) as f:
    lines = f.read().splitlines()

pos = np.array([int(l.split(':')[1]) for l in lines])

# initialize winning universes counters for both players 
# 0 => first player, 1 = second player
winnings = np.array([0, 0], dtype=np.int64)

# initialize universe map:
# dim 0: current positions of first player (0..9 => 1..10)
# dim 1: current positions of seconde player (0..9 => 1..10)
# dim 2: current points of first player (0..20)
# dim 3: current points of second player (0..20)
# value: count of universes in this configuration
u_c = np.zeros((10, 10, 21, 21), dtype=np.int64)
# we start in our universe: count 1
# positions given above (minus one for mapping)
# points both zero
u_c[pos[0]-1, pos[1]-1, 0, 0] = 1

# we start at player 1 (0)
pid = 0

# quick'n'dirty binomial map...was too stressed to simply write it down:
# calculate all split universe results as list
roll_cnt = np.array([3+sum((i,j,k)) for i in range(3) for j in range(3) for k in range(3)])
# count for all possible outcomes (dice count of 3...9)
rolls_cnt = np.array([(roll_cnt==d).sum() for d in range(3,10)])
# corresponding dice counts
rolls_val = np.arange(3,10,1, dtype=np.int64)

# loop until all possible universes have a winner (no entries in map)
while (u_c>0).sum() > 0:
    # create empty new universe map to fill with next state
    u_n = np.zeros((10, 10, 21, 21), dtype=np.int64)
    
    # get all existing configurations (non zero number of universes with that combi of pos/pts)
    nnz = np.nonzero(u_c)
    # create lut of the indices, so we know pos/points of first/second player
    nz = np.stack(nnz)
    # extract number of universes for all these combinations
    nn = u_c[nnz]
    # loop over combinations (combination index i)
    for i in range(nz.shape[1]):
        # get number of universes with i-th combination
        n = nn[i]
        
        # depending on current player
        # we operate on different indices
        if pid == 0:
            # first player
            # get position from index nz[0,i]
            # add one because of 0..9<=>1..10 map
            # add possible dice sums
            # do modulo 10 operation with offset 1: 1+((x-1)%10)
            pos = 1 + (((nz[0,i]+1 + rolls_val)-1) % 10)
            # get current points of player in this combi
            # and add the new positons
            pts = nz[2,i] + pos
            # go through all 7 possible outcomes (dice is 3...9)
            for j in range(7):
                # this outcome doesn't result in win?
                if pts[j] < 21:
                    # add the number of universes to next combination state
                    # second player is not changed (nz[1,i], nz[3,i])
                    # first player is defined by new positon and points
                    # pos[j]-1 and pts[j]
                    # multiply by original number of universes
                    u_n[pos[j]-1, nz[1,i], pts[j], nz[3,i]] += n*rolls_cnt[j]
                else:
                    # otherwise first player won => add to winning list
                    winnings[pid] += n*rolls_cnt[j]
                    
        else:
            # same as for first player, roles reversed
            pos = 1 + (((nz[1,i]+1 + rolls_val)-1) % 10)
            pts = nz[3,i] + pos
            for j in range(7):
                if  pts[j] < 21:
                    u_n[nz[0,i], pos[j]-1, nz[2,i], pts[j]] += n*rolls_cnt[j]
                else:
                    winnings[pid] += n*rolls_cnt[j]
    
    # declare new created univere map as the current one
    u_c = u_n
    # switch player
    pid = 1-pid

# get the bigger on of the winning list
result = winnings.max()

print('Puzzle solution: {:d}'.format(result))