# -*- coding: utf-8 -*-
"""
Created on Tue Dec 21 21:45:50 2021

@author: PII_DEI
"""
import pandas as pd
import numpy as np
import scipy.signal as ssig

fname = 'example.txt'
fname = 'input.txt'

with open(fname) as f:
    lines = f.read().splitlines()


pos = np.array([int(l.split(':')[1]) for l in lines])

points = np.array([0,0], dtype=np.int64)

winnings = np.array([0, 0], dtype=np.int64)

# initialize universe
u_c = np.zeros((10, 10, 21, 21), dtype=np.int64)
u_c[pos[0]-1, pos[1]-1, 0, 0] = 1

pid = 0

roll_cnt = np.array([3+sum((i,j,k)) for i in range(3) for j in range(3) for k in range(3)])
rolls_cnt = np.array([(roll_cnt==d).sum() for d in range(3,10)])
rolls_val = np.arange(3,10,1, dtype=np.int64)

# %%

while (u_c>0).sum() > 0:
    u_n = np.zeros((10, 10, 21, 21), dtype=np.int64)
    
    nnz = np.nonzero(u_c)
    nz = np.stack(nnz)
    nn = u_c[nnz]
    for i in range(nz.shape[1]):
        n = nn[i]
        
        if pid == 0:
            pos = 1 + (((nz[0,i]+1 + rolls_val)-1) % 10)
            pts = nz[2,i] + pos
            for j in range(7):
                if pts[j] < 21:
                    u_n[pos[j]-1, nz[1,i], pts[j], nz[3,i]] += n*rolls_cnt[j]
                else:
                    winnings[pid] += n*rolls_cnt[j]
                    
        else:
            pos = 1 + (((nz[1,i]+1 + rolls_val)-1) % 10)
            pts = nz[3,i] + pos
            for j in range(7):
                if  pts[j] < 21:
                    u_n[nz[0,i], pos[j]-1, nz[2,i], pts[j]] += n*rolls_cnt[j]
                else:
                    winnings[pid] += n*rolls_cnt[j]
    
    u_c = u_n
    pid = 1-pid

result = winnings.max()

print('Puzzle solution: {:d}'.format(result))