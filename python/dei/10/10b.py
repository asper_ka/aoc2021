# -*- coding: utf-8 -*-
"""
Created on Fri Dec 10 05:58:08 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np

fname = 'example.txt'
fname = 'input.txt'

data = pd.read_csv(
    fname, 
    sep = ' ', 
    header=None,
    dtype='str'
    )

data = data[0].to_list()

omatch = {
    ')': '(',
    ']': '[',
    '}': '{',
    '>': '<'
    }

cmatch = {
    '(': ')',
    '[': ']',
    '{': '}',
    '<': '>'
    }

ac_score_lut = {
    ')': 1,
    ']': 2,
    '}': 3,
    '>': 4
    }

pt_lst = []


for comm in data:
    cstack = []
    is_corr = False
    for c in comm:
        if c in '([<{':
            cstack.append(c)
        elif c in ')]>}':
            cc = cmatch[cstack.pop()]
            
            # discard corrupted line
            if cc != c:                
                is_corr = True
                break
        
    if not is_corr and len(cstack)>0:
        # ac
        cstack = np.array(cstack)[::-1]
        score = 0
        for c in cstack:
            cc = cmatch[c]
            score *= 5
            score += ac_score_lut[cc]
        pt_lst.append(score)
        
result = int(np.median(np.array(pt_lst)))

print('Puzzle solution: {:d}'.format(result))