# -*- coding: utf-8 -*-
"""
Created on Fri Dec 10 05:58:08 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np

fname = 'example.txt'
fname = 'input.txt'

data = pd.read_csv(
    fname, 
    sep = ' ', 
    header=None,
    dtype='str'
    )

data = data[0].to_list()

omatch = {
    ')': '(',
    ']': '[',
    '}': '{',
    '>': '<'
    }

cmatch = {
    '(': ')',
    '[': ']',
    '{': '}',
    '<': '>'
    }

err_score = {
    ')': 0,
    ']': 0,
    '}': 0,
    '>': 0
    }

for comm in data:
    cstack = []
    for c in comm:
        if c in '([<{':
            cstack.append(c)
        elif c in ')]>}':
            cc = cmatch[cstack.pop()]
            if cc != c:
                err_score[c] += 1
                break
        
result = err_score[')']*3 + err_score[']']*57 + err_score['}']*1197 + err_score['>'] * 25137

print('Puzzle solution: {:d}'.format(result))