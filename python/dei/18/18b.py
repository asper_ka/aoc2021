# -*- coding: utf-8 -*-
"""
Created on Sat Dec 18 06:02:47 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np

fname = 'example1.txt'
fname = 'example3.txt'
fname = 'input.txt'



def sfn_a(sfn, sfn_x = None, l=0, s=0, i=[]):
    if sfn_x is None:
        sfn_x = {
            #'o': sfn,
            'i': [],
            'd': [],
            'l': [],
            's': []
            }
        
    if isinstance(sfn, list):
        sfn_x = sfn_a(sfn[0], sfn_x, l+1, 0, i+[0])
        sfn_x = sfn_a(sfn[1], sfn_x, l+1, 1, i+[1])
    else:
        if l > 5:
            print('L>5!!!')
        sfn_x['i'].append(i)
        sfn_x['d'].append(sfn)
        sfn_x['l'].append(l)
        sfn_x['s'].append(s)   
        
    if l > 0:
        return sfn_x
    else:
        #sfn_x['i'] = np.array(sfn_x['i'], dtype=object)
        sfn_x['d'] = np.array(sfn_x['d'])
        sfn_x['l'] = np.array(sfn_x['l'])
        sfn_x['s'] = np.array(sfn_x['s'])        
        return sfn_x

def sfn_b(sfn_x):    
    l = sfn_x['l'][0]
    s = l * '['
    x = l * [0]
    s += str(sfn_x['d'][0])
    
    x[-1] = 1
    s += ', '
    i = 1
        
    for i in range(1, len(sfn_x['d'])):
        y = sfn_x['i'][i]
        
        while len(x) < len(y):
            x.append(0)
            s += '['
        
        s += str(sfn_x['d'][i])
        while len(x)>0 and x[-1] == 1:
            x = x[:-1]
            s += ']'
        
        if len(x)>0:
            x[-1] += 1
            s += ', '
        
    return eval(s)
    

def sfn_x_explode(sfn_x):
    while any(sfn_x['l'] == 5):
        exl = np.nonzero(sfn_x['l']>=5)[0][0]
        exr = exl+1
        
        if exl > 0:
            sfn_x['d'][exl-1] += sfn_x['d'][exl]
        if exr < sfn_x['d'].size-1:
            sfn_x['d'][exr+1] += sfn_x['d'][exr]
        
        sfn_x['d'] = np.delete(sfn_x['d'], exr)
        sfn_x['l'] = np.delete(sfn_x['l'], exr)
        sfn_x['s'] = np.delete(sfn_x['s'], exr)
        del sfn_x['i'][exr]
        
        sfn_x['d'][exl] = 0
        sfn_x['l'][exl] -= 1
        sfn_x['s'][exl] = sfn_x['i'][exl][-2]
        sfn_x['i'][exl] = sfn_x['i'][exl][:-1]
    return sfn_x
    
def sfn_x_split(sfn_x):
    while any(sfn_x['d'] >= 10):
        si = np.nonzero(sfn_x['d']>=10)[0][0]
        
        vl = sfn_x['d'][si] // 2
        vr = sfn_x['d'][si] - vl
        
        il = sfn_x['i'][si] + [0]
        ir = sfn_x['i'][si] + [1]
        
        l = sfn_x['l'][si] + 1
                
        sfn_x['d'][si] = vl
        sfn_x['l'][si] = l
        sfn_x['s'][si] = 0
        sfn_x['i'][si] = il
        
        si += 1
        
        sfn_x['d'] = np.insert(sfn_x['d'], si, vr)
        sfn_x['l'] = np.insert(sfn_x['l'], si, l)
        sfn_x['s'] = np.insert(sfn_x['s'], si, 1)
        sfn_x['i'].insert(si, ir)
    
        sfn_x = sfn_x_explode(sfn_x)
    
    return sfn_x

def sfn_x_reduce(sfn_x):
    sfn_x = sfn_x_explode(sfn_x)
    sfn_x = sfn_x_split(sfn_x)
    return sfn_x

def sfn_add(sfn_x, sfn_y):
    if isinstance(sfn_x, list):
        sfn_x = sfn_a(sfn_x)
    if isinstance(sfn_y, list):
        sfn_y = sfn_a(sfn_y)
    i1 = [[0] + i for i in sfn_x['i']]
    i2 = [[1] + i for i in sfn_y['i']]
    #print(i1)
    #print(i2)
    sfn_r = {
        'i': i1 + i2,
        'd': np.hstack((sfn_x['d'], sfn_y['d'])),
        'l': np.hstack((sfn_x['l']+1, sfn_y['l']+1)),
        's': np.hstack((sfn_x['s'], sfn_y['s']))
        }
    return sfn_x_reduce(sfn_r)

def sfn_sum(l):
    if len(l)<2:
        return l
    lacc = sfn_add(l[0], l[1])
    for i in range(2, len(l)):
        lacc = sfn_add(lacc, l[i])
    return lacc

def sfn_mag(sfn_x):
    if isinstance(sfn_x, dict):
        sfn = sfn_b(sfn_x)
    else:
        sfn = sfn_x
    
    if isinstance(sfn, list):
        return 3*sfn_mag(sfn[0]) + 2*sfn_mag(sfn[1])
    else:
        return sfn





data = pd.read_csv(
    fname, 
    sep = ' ', 
    header=None,
    dtype='str'
    ).to_numpy()[:,0]

nlines = [
    eval(d) for d in data
    ]

mag_mat = np.empty((len(nlines), len(nlines)), dtype=np.int64)

for i in range(len(nlines)):
    for j in range(len(nlines)):
        mag_mat[i, j] = sfn_mag(sfn_add(nlines[i], nlines[j]))

print(mag_mat.max())
