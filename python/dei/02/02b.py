# -*- coding: utf-8 -*-
"""
Created on Thu Dec  2 05:59:34 2021

@author: PII_DEI
"""

import pandas as pd
# import numpy as np
# import scipy.signal as sig

hpos = 0
depth = 0
aim = 0

def move(data_row):
    global hpos, depth, aim
    if data_row[0] == 'forward':
        hpos += data_row[1]
        depth += aim * data_row[1]
    elif data_row[0] == 'down':
        aim += data_row[1]
    elif data_row[0] == 'up':
        aim -= data_row[1]
        
data = pd.read_csv('..\\02\\input.txt', header=None, sep=' ')

data.apply(move, axis=1)

print('hpos*depth = {:d}'.format(hpos*depth))
