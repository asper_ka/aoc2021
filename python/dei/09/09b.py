# -*- coding: utf-8 -*-
"""
Created on Thu Dec  9 05:58:52 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np

fname = 'example.txt'
fname = 'input.txt'

data = pd.read_csv(
    fname, 
    sep = ' ', 
    header=None,
    dtype='str'
    )

data = data[0].to_list()

w = len(data[0])
h = len(data)

data = np.array([[int(d) for d in r] for r in data])

top_low = np.vstack(
    ([[True]*w], (data[1:,:] - data[:-1,:]) < 0))

bot_low = np.vstack(
    ((data[:-1,:] - data[1:,:]) < 0, [[True]*w]))

left_low = np.hstack(
    ([[True]]*h, (data[:,1:] - data[:,:-1]) < 0))

right_low = np.hstack(
    ((data[:,:-1] - data[:,1:]) < 0, [[True]]*h) )

is_low = top_low & bot_low & left_low & right_low


basin_seeds = np.nonzero(is_low)
num_basins = len(basin_seeds[0])


basin_id_map = np.full(data.shape, -1, dtype=np.int64)
basin_done = np.array([[False]*w]*h)

basin_id_map[basin_seeds] = list(range(num_basins))
basin_done[basin_seeds] = True
basin_done[data == 9] = True

def basin_mark(id, x, y):
    basin_id_map[y, x] = id
    basin_done[y, x] = True
    
    if (y > 0) and (not basin_done[y-1,x]) and data[y-1, x]>=data[y,x]:
        basin_mark(id, x, y-1)        
        
    if (y < h-1) and (not basin_done[y+1,x]) and data[y+1, x]>=data[y,x]:
        basin_mark(id, x, y+1)
        
    if (x > 0) and (not basin_done[y,x-1]) and data[y, x-1]>=data[y,x]:
        basin_mark(id, x-1, y)  
        
    if (x < w-1) and (not basin_done[y,x+1]) and data[y, x+1]>=data[y,x]:
        basin_mark(id, x+1, y)
        
basin_sizes = []
for i in range(num_basins):
    basin_mark(i, basin_seeds[1][i], basin_seeds[0][i])
    basin_sizes.append( (basin_id_map==i).sum().sum() )

basin_sizes = np.array(basin_sizes)

basin_sizes.sort()

result = np.product(basin_sizes[-3:])

print('Puzzle solution: {:d}'.format(result))