# -*- coding: utf-8 -*-
"""
Created on Thu Dec  9 05:58:52 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np

fname = 'example.txt'
fname = 'input.txt'

data = pd.read_csv(
    fname, 
    sep = ' ', 
    header=None,
    dtype='str'
    )

data = data[0].to_list()

w = len(data[0])
h = len(data)

data = np.array([[int(d) for d in r] for r in data])

top_low = np.vstack(
    ([[True]*w], (data[1:,:] - data[:-1,:]) < 0))

bot_low = np.vstack(
    ((data[:-1,:] - data[1:,:]) < 0, [[True]*w]))

left_low = np.hstack(
    ([[True]]*h, (data[:,1:] - data[:,:-1]) < 0))

right_low = np.hstack(
    ((data[:,:-1] - data[:,1:]) < 0, [[True]]*h) )

is_low = top_low & bot_low & left_low & right_low

risk_level = (1+data[is_low]).sum()


result = risk_level

print('Puzzle solution: {:d}'.format(result))