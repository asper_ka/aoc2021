# -*- coding: utf-8 -*-
"""
Created on Thu Dec  9 09:51:02 2021

@author: PII_DEI
"""

import numpy as np
import matplotlib.pyplot as plt

from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm
    

def plt_basin_data(fname = 'input.txt'):    
    
    def basin_mark(id, x, y):
        basin_id_map[y, x] = id
        basin_done[y, x] = True
        
        if (y > 0) and (not basin_done[y-1,x]) and data[y-1, x]>=data[y,x]:
            basin_mark(id, x, y-1)        
            
        if (y < h-1) and (not basin_done[y+1,x]) and data[y+1, x]>=data[y,x]:
            basin_mark(id, x, y+1)
            
        if (x > 0) and (not basin_done[y,x-1]) and data[y, x-1]>=data[y,x]:
            basin_mark(id, x-1, y)  
            
        if (x < w-1) and (not basin_done[y,x+1]) and data[y, x+1]>=data[y,x]:
            basin_mark(id, x+1, y)
    
    
    data = np.loadtxt(fname, dtype='str')

    w = len(data[0])
    h = len(data)

    data = np.array([[int(d) for d in r] for r in data])

    top_low = np.vstack(
        ([[True]*w], (data[1:,:] - data[:-1,:]) < 0))
    
    bot_low = np.vstack(
        ((data[:-1,:] - data[1:,:]) < 0, [[True]*w]))
    
    left_low = np.hstack(
        ([[True]]*h, (data[:,1:] - data[:,:-1]) < 0))
    
    right_low = np.hstack(
        ((data[:,:-1] - data[:,1:]) < 0, [[True]]*h) )

    is_low = top_low & bot_low & left_low & right_low
    
    
    basin_seeds = np.nonzero(is_low)
    num_basins = len(basin_seeds[0])
    
    
    basin_id_map = np.full(data.shape, -1, dtype=np.int64)
    basin_done = np.array([[False]*w]*h)
    
    basin_id_map[basin_seeds] = list(range(num_basins))
    basin_done[basin_seeds] = True
    basin_done[data == 9] = True
    
            
    basin_sizes = []
    for i in range(num_basins):
        basin_mark(i, basin_seeds[1][i], basin_seeds[0][i])
        basin_sizes.append( (basin_id_map==i).sum().sum() )
    
    basin_sizes = np.array(basin_sizes)
    
    basin_order = np.argsort(basin_sizes)[::-1]
    
    basin_id_bool_maps = []
    for i in range(num_basins):
        bid = basin_order[i]
        basin_id_bool_maps.append((basin_id_map == bid).copy())
        
    for bid in range(num_basins):
        basin_id_map[basin_id_bool_maps[bid]] = bid
    
    
    img_data = 0.1 + 0.8 * (data / 9.0)
    img_data[data == 9] = 1.0
    
    img_data = np.tile(img_data[:, :, None], [1, 1, 3])
    
    img_data[basin_id_map == 0, 0] = 0.0
    img_data[basin_id_map == 1, 1] = 0.0
    img_data[basin_id_map == 2, 2] = 0.0
    
    #img_data[data == 9, 1] = 0.9
    #img_data[data == 9, 2] = 0.9
    
    plt.figure(figsize=(16,16), facecolor='k')
    plt.imshow(img_data)
    plt.axis('off')
    plt.show()
    
    
    #cset = ax.contourf(X, Y, Z, zdir='z', offset=np.min(Z), cmap=cm.ocean)
    
    
    #fig.colorbar(surf, ax=ax, shrink=0.5, aspect=5)
    
    x = np.arange(0, w, 1)
    y = np.arange(0, h, 1)
    X,Y = np.meshgrid(x,y)
    Z = (data).reshape(X.shape).copy()
        
    fig = plt.figure(figsize=(16,10))
    ax = plt.axes(projection='3d') #fig.add_subplot(111, projection='3d')
        
    surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, alpha=0.8, cmap=cm.ocean)
       
    ax.set_xlabel('X')
    ax.set_xlim(0, w-1)
    ax.set_ylabel('Y')
    ax.set_ylim(0, h-1)
    ax.set_zlabel('Z')
    ax.set_zlim(np.min(Z), np.max(Z))
    ax.set_box_aspect((np.ptp(X), np.ptp(Y), np.ptp(Z)))
    
    plt.axis('off')
    ax.view_init(50, 35)
    
    plt.savefig('09_3d.png', facecolor='k', transparent=True)
    plt.show()
    
    
    return basin_id_map

plt_basin_data()