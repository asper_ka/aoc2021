# -*- coding: utf-8 -*-
"""
Created on Wed Dec  1 17:41:28 2021

@author: PII_DEI
"""

import pandas as pd

data = pd.read_csv('input.txt', header=None)

terain_change = (data.diff() > 0)
increase_count = terain_change.sum().iloc[0]

print('{:d} measurements are larger than the previous measurement!'.format(
    increase_count))