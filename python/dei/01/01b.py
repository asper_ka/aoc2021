# -*- coding: utf-8 -*-
"""
Created on Wed Dec  1 17:41:28 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np
import scipy.signal as sig

data = pd.read_csv('input.txt', header=None)

som_data = pd.DataFrame(
    sig.correlate(data, np.ones((3,1)), mode='valid', method='direct'),
    index=data.index[1:-1])

terain_change = (som_data.diff() > 0)
increase_count = terain_change.sum().iloc[0]

print('{:d} sum of measurements are larger than the previous measurement!'.format(
    increase_count))