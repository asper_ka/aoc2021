# -*- coding: utf-8 -*-
"""
Created on Thu Dec 16 05:58:06 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np

fname = 'example1.txt'
fname = 'example2.txt'
fname = 'example3.txt'
fname = 'input.txt'



data = pd.read_csv(
    fname, 
    sep = ' ', 
    header=None,
    dtype='str'
    ).to_numpy()

data = data[0,0]

# data = '8A004A801A8002F478'
# data = '620080001611562C8802118E34'
# data = 'C0015000016115A2E0802F182340'
# data = 'A0016C880162017C3686B18A3D4780'

# data  = 'C200B40A82'
# data  = '04005AC33890'
# data = '9C0141080250320F1802104A08'
# data = '9C005AC2F8F0'
# data = 'F600BC2D8F'
# data = 'D8005AC2A8F0'
# data = 'CE00C43D881120'
# data = '880086C3E88112'
# data = '04005AC33890'

numbits = len(data)*4

numc = int(np.ceil(numbits / 64))

bitstream = ''
for i in range(numc-1):
    dval = int(data[i*16:(i+1)*16], 16)
    bitstream += bin(dval)[2:].zfill(64)

if numbits % 64 > 0:
    dval = int(data[(numc-1)*16:], 16)
    bitstream += bin(dval)[2:].zfill(numbits % 64)
    
    
    
def readpacket(s, i):
    v = int(s[i:i+3],2)
    t = int(s[i+3:i+6],2)
    # print(v,t)
    
    if t == 4:
        plst = []
        
        g = 1
        n = 0
        val = 0
        while g != 0:
            g = int(s[i+6+n*5])
            val = val*16 + int(s[i+6+n*5+1:i+6+(n+1)*5],2)
            n += 1
        
        return v, i+6+n*5, val
    
    else:
        tver = v
        
        lt = int(s[i+6])
        numlst = []
        
        ii = i
        
        if lt == 0:
            l = int(s[i+7:i+22], 2)
            
            ii = i+22
            while ii < i+22+l:
                subp = readpacket(s, ii)
                ii = subp[1]
                tver += subp[0]
                numlst.append(subp[2])
            
        else:
            l = int(s[i+7:i+18], 2)
            
            ii = i+18
            for k in range(l):
                subp = readpacket(s, ii)
                ii = subp[1]
                tver += subp[0]
                numlst.append(subp[2])
                
        # doop
        numlst = np.array(numlst)
        
        if t == 0:
            val = numlst.sum()
        elif t == 1:
            val = numlst.prod()
        elif t == 2:
            val = numlst.min()
        elif t == 3:
            val = numlst.max()
        elif t == 5:
            val = 1 if numlst[0] > numlst[1] else 0
        elif t == 6:
            val = 1 if numlst[0] < numlst[1] else 0
        elif t == 7:
            val = 1 if numlst[0] == numlst[1] else 0
        print(val)
        
        return v, ii, val
            
        

test = readpacket(bitstream, 0)
    

result = test[2]

print('Puzzle solution: {:d}'.format(result))