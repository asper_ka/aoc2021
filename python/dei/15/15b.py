# -*- coding: utf-8 -*-
"""
Created on Wed Dec 15 05:58:23 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np
import pqueue

fname = 'example.txt'
fname = 'input.txt'



data = pd.read_csv(
    fname, 
    sep = ' ', 
    header=None,
    dtype='str'
    ).to_numpy()

data = np.array([[int(d) for d in r] for r in data[:,0]])

h, w = data.shape

bigdata = np.empty((h*5, w*5), dtype=np.int64)
for y in range (5):
    for x in range(5):
        bigdata[y*h:(y+1)*h, x*w:(x+1)*w] = data + x + y

data = np.mod(bigdata-1, 9)+1
h, w = data.shape

xmax = w-1
ymax = h-1

MAXINT = np.int64(1<<63-1)


vertices_map = [[(y,x) for x in range(w)] for y in range(h)]
weights = data.reshape((h*w,))

edges = []
lengths = []

for y in range(h):
    ee = []
    for x in range(w):
        e = []
        if x>0:
            e.append(y*w+(x-1))
        if y>0:
            e.append((y-1)*w+x)
            
        if x<xmax:
            e.append(y*w+(x+1))
            
        if y<ymax:
            e.append((y+1)*w+x)
        
        e = np.array(e, dtype=np.int64)
        edges.append(e)
        lengths.append(weights[e])
        
# %%

def AStar(start = 0, goal = w*h-1):
    
    def hfnc(n):
        y = (n//w)
        x = n-y*w
        return (h-y) + (w-x)
    
    closedList = np.full((w*h,), False)
    gList = np.full((w*h,), MAXINT, dtype=np.int64)
    gList[start] = 0
                    

    openList = pqueue.PQueue()
    openList.enqueue(start, hfnc(start))
    
    
    
    def expandNode(cn):
        for i in range(edges[cn].size):
            s = edges[cn][i]
            if closedList[s]:
                continue
            
            tg = gList[cn] + lengths[cn][i]
            if openList.contains(s) and tg >= gList[s]:
                continue
            
            gList[s] = tg
            f = tg + hfnc(s)
            if openList.contains(s):
                openList.updateKey(s, f)
            else:
                openList.enqueue(s, f)
    
    ind = 0
    while True:
        itm = openList.removeMin()
        cn = itm.n
        if (ind % 1000) == 0:
            y = (cn//w)
            x = cn-y*w
            print("{:3d} {:3d}: {:5d} ({:8d} + {:8d} / {:8d} )".format(
                x, y, itm.f, openList._cnt, openList._delcnt, closedList.sum()))
        ind += 1
        
        if cn == goal:
            return gList[cn]
        
        closedList[cn] = True
        
        expandNode(cn)
                
        if openList.isEmpty():
            break

    return -1
    

print('Puzzle solution: {:d}'.format(AStar()))