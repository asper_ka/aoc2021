# -*- coding: utf-8 -*-
"""
Created on Wed Dec 15 05:58:23 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np

fname = 'example.txt'
fname = 'input.txt'



data = pd.read_csv(
    fname, 
    sep = ' ', 
    header=None,
    dtype='str'
    ).to_numpy()

data = np.array([[int(d) for d in r] for r in data[:,0]])

h, w = data.shape
xmax = w-1
ymax = h-1

# x = 0
# y = 0

vertices_map = [[(y,x) for x in range(w)] for y in range(h)]
weights = data.reshape((h*w,))

edges = []
lengths = []

for y in range(h):
    ee = []
    for x in range(w):
        e = []
        if x>0:
            e.append(y*w+(x-1))
        if y>0:
            e.append((y-1)*w+x)
            
        if x<xmax:
            e.append(y*w+(x+1))
            
        if y<ymax:
            e.append((y+1)*w+x)
        
        e = np.array(e, dtype=np.int64)
        edges.append(e)
        lengths.append(weights[e])

def Dijkstra():
    dist = np.full((w*h,), np.inf)
    prev = np.full((w*h,), -1, dtype=np.int64)
    Q = list(range(w*h))
    
    dist[0*w+0] = 0
    
    while len(Q)>0:
        ui = np.argmin(dist[Q])
        u = Q[ui]
        del Q[ui]
        
        if u == h*w-1:
            break
        
        for vi in range(len(edges[u])):
            v = edges[u][vi]
            # print(u,v)
            if v in Q:
                alt = dist[u] + lengths[u][vi]
                if alt < dist[v]:
                    # print(alt)
                    dist[v] = alt
                    prev[v] = u
    return int(dist[-1])
    

print('Puzzle solution: {:d}'.format(Dijkstra()))