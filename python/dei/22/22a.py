# -*- coding: utf-8 -*-
"""
Created on Wed Dec 22 13:31:24 2021

@author: PII_DEI
"""

import numpy as np

fname = 'example1.txt'
fname = 'example2.txt'
fname = 'input.txt'

with open(fname) as f:
    lines = f.read().splitlines()
    
seq_states = np.array([l.split(' ')[0] == 'on' for l in lines])

cubes = np.array([[[int(n) for n in s[2:].split('..')] for s in l.split(' ')[1].split(',')] for l in lines])

cube_filter = [6==((c>=-50) & (c<=50)).sum() for c in cubes]
cubes = cubes[cube_filter] + 50
seq_states = seq_states[cube_filter]
# cubes = cubes.clip(-50, 50) + 50


reactor = np.zeros((101, 101, 101), dtype=bool)

for i in range(len(seq_states)):
    reactor[
        cubes[i,0,0]:cubes[i,0,1]+1,
        cubes[i,1,0]:cubes[i,1,1]+1,
        cubes[i,2,0]:cubes[i,2,1]+1] = seq_states[i]
    

result = reactor.sum()

print('Puzzle solution: {:d}'.format(result))