# -*- coding: utf-8 -*-
"""
Created on Wed Dec 22 13:31:24 2021

@author: PII_DEI
"""

import numpy as np

fname = 'example3.txt'
fname = 'input.txt'

with open(fname) as f:
    lines = f.read().splitlines()
    
seq_states = np.array([l.split(' ')[0] == 'on' for l in lines])

cubes = np.array([[[int(n) for n in s[2:].split('..')] for s in l.split(' ')[1].split(',')] for l in lines])


xlst_0 = np.unique(cubes[:,0,0].flatten())
xlst_1 = np.unique(cubes[:,0,1].flatten())+1
xlst = np.unique(np.hstack((xlst_0, xlst_1)))

ylst_0 = np.unique(cubes[:,1,0].flatten())
ylst_1 = np.unique(cubes[:,1,1].flatten())+1
ylst = np.unique(np.hstack((ylst_0, ylst_1)))

zlst_0 = np.unique(cubes[:,2,0].flatten())
zlst_1 = np.unique(cubes[:,2,1].flatten())+1
zlst = np.unique(np.hstack((zlst_0, zlst_1)))

dx = xlst[1:]-xlst[:-1]
dy = ylst[1:]-ylst[:-1]
dz = zlst[1:]-zlst[:-1]

sizes = np.ones((dx.size, dy.size, dz.size), dtype=np.int64)

sizes *= dx.reshape((dx.size,1,1))
sizes *= dy.reshape((1,dy.size,1))
sizes *= dz.reshape((1,1,dz.size))

reactor = np.zeros((xlst.size-1, ylst.size-1, zlst.size-1), dtype=bool)

for i in range(len(seq_states)):
    x1 = np.nonzero(xlst==cubes[i,0,0])[0][0]
    x2 = np.nonzero(xlst==cubes[i,0,1]+1)[0][0]
    
    y1 = np.nonzero(ylst==cubes[i,1,0])[0][0]
    y2 = np.nonzero(ylst==cubes[i,1,1]+1)[0][0]
    
    z1 = np.nonzero(zlst==cubes[i,2,0])[0][0]
    z2 = np.nonzero(zlst==cubes[i,2,1]+1)[0][0]
        
    
    reactor[x1:x2, y1:y2, z1:z2] = seq_states[i]
    

result = sizes[reactor].sum()

print('Puzzle solution: {:d}'.format(result))