# -*- coding: utf-8 -*-
"""
Created on Sun Dec 19 07:59:17 2021

@author: PII_DEI
"""

import numpy as np

scanners = np.loadtxt('scanners.csv', dtype=np.int64, delimiter=',')
nscan = scanners.shape[0]

maxdist = 0
sol = [-1, -1]
for a in range(nscan):
    for b in range(a+1, nscan):
        d = abs(scanners[a,:] - scanners[b,:]).sum()
        if d > maxdist:
            maxdist = d
            sol = [a, b]
            
print(maxdist)