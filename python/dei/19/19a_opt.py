# -*- coding: utf-8 -*-
"""
Created on Sun Dec 19 13:49:47 2021

@author: PII_DEI
"""
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 19 05:54:09 2021

@author: PII_DEI
"""

import numpy as np

fname = 'example1.txt'
fname = 'example2.txt'
fname = 'example3.txt'
fname = 'input.txt'


with open(fname) as f:
    lines = f.read().splitlines()
    
lens = np.array([len(l) for l in lines])

eobs = np.nonzero(lens == 0)[0]
eobs = np.append(eobs, len(lines))
eobs = np.insert(eobs, 0, -1)

scans = []
for i in range(len(eobs)-1):
    scans.append(
        np.array([
            l.split(',') for l in lines[eobs[i]+2:eobs[i+1]]],
            dtype=np.int64))


nscan = len(scans) 
scanner_rel_pos = { 0: np.array([0,0,0], dtype=np.int64) }
correct = np.full((nscan,), False)
correct[0] = True

scanner_relations = np.eye(nscan, dtype=np.int64)


ORI = {
       'xs': np.array([
           [1,0,0],[-1,0,0],
           [0,1,0],[0,-1,0],
           [0,0,1],[0,0,-1]
           ], dtype=np.int64),
       'ys': np.array([
           [2,3,4,5],
           [2,3,4,5],
           [0,1,4,5],
           [0,1,4,5],
           [0,1,2,3],
           [0,1,2,3]
           ], dtype=np.int64)
       }
    
def change_o(data, o):
    xv = ORI['xs'][o%6,:]
    yv = ORI['xs'][ORI['ys'][o%6,o//6],:]
    zv = np.cross(xv, yv)
    rm = np.vstack((xv, yv, zv)).T
    #print(rm)
    return (rm @ data.T).T


def match_scanner_pair(id0, id1):
    # if id0 in scanner_rel_pos:
    #     s0 = scans[id0] - [scanner_rel_pos[id0]]
    # else:
    #     s0 = scans[id0]
        
    # if id1 in scanner_rel_pos:
    #     s1 = scans[id1] - [scanner_rel_pos[id1]]
    # else:
    #     s1 = scans[id1]
    s0 = scans[id0]
    s1 = scans[id1]
    
    s0l = s0.shape[0]
    s1l = s1.shape[0]
    m = -1
    scnt = [0, 0, 0, 0]
    for o in range(24):
        s1r = change_o(s1, o)
        
        for i in range(s0l):
            ref0 = s0[i, :]
            for j in range(s1l):
                ref1 = s1r[j, :]
                
                dr = ref1 - ref0
                                
                s1rr = s1r - [dr]
                
                flt = (np.abs(s1rr) <= 1000).all(axis=1)
                
                if flt.sum() < 12:
                    scnt[0]+=1
                    continue
                
                c = np.equal.outer(s0[:,0], s1rr[flt,0])
                if c.sum()<12:
                    scnt[1]+=1
                    continue
                
                c &= np.equal.outer(s0[:,1], s1rr[flt,1])
                if c.sum()<12:
                    scnt[2]+=1
                    continue
                
                c &= np.equal.outer(s0[:,2], s1rr[flt,2])
                if c.sum()<12:
                    scnt[3]+=1
                    continue
                
                m = 12
                print('  MATCH, SKIPPED {:d}/{:d}/{:d}/{:d}'.format(*scnt))
                break
                        
                    
            if m >= 12:
                break
        if m >= 12:
            break
    if m >= 12:
        scanner_relations[id1, :] += scanner_relations[id0, :]
        print( s0[c.any(axis=1),:] )
        return True, -dr, s1rr + dr#, s0[c.any(axis=1),:]
    else:
        print('         SKIPPED {:d}/{:d}/{:d}/{:d}'.format(*scnt))
        return False, None, None#, None




# def test():
#     a=0 
#     b=1    
#     ab = match_scanner_pair(a, b)
#     return ab

# cprof.run('tst = test()')
# test()
    
# %%

while not all(correct):    
    for a in range(nscan):
        for b in range(nscan):
            if correct[a] and not correct[b]:
                print(a,b)
                ab = match_scanner_pair(a, b)
                if ab[0]:
                    scans[b] = ab[2]
                    scanner_rel_pos[b] = ab[1]
                    correct[b] = True
            elif correct[b] and not correct[a]:
                print(b,a)
                ab = match_scanner_pair(b, a)
                if ab[0]:
                    scans[a] = ab[2]
                    scanner_rel_pos[a] = ab[1]
                    correct[a] = True
    print("{:d} correct, {:d} missing".format(
        correct.sum(), nscan-correct.sum())
        )
    
scanner_abs_pos = scanner_relations @ np.array([scanner_rel_pos[i] for i in range(nscan)])
abs_scans = [
    scans[i] + scanner_abs_pos[i]
    for i in range(nscan)
    ]



beacons = np.unique(np.vstack(abs_scans), axis=0) 

np.savetxt('scanners.csv', scanner_abs_pos, fmt='%d', delimiter = ',')
np.savetxt('beacons.csv', beacons, fmt='%d', delimiter = ',')


result = beacons.shape[0]

print('Puzzle solution: {:d}'.format(result))