# -*- coding: utf-8 -*-
"""
Created on Sun Dec 19 05:54:09 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np

fname = 'example1.txt'
fname = 'example2.txt'
fname = 'example3.txt'
fname = 'input.txt'


with open(fname) as f:
    lines = f.read().splitlines()
    
lens = np.array([len(l) for l in lines])

eobs = np.nonzero(lens == 0)[0]
eobs = np.append(eobs, len(lines))
eobs = np.insert(eobs, 0, -1)

scanners = []
for i in range(len(eobs)-1):
    scanners.append(
        np.array([
            l.split(',') for l in lines[eobs[i]+2:eobs[i+1]]],
            dtype=np.int64))
    
ORI = {
       'xs': np.array([
           [1,0,0],[-1,0,0],
           [0,1,0],[0,-1,0],
           [0,0,1],[0,0,-1]
           ], dtype=np.int64),
       'ys': np.array([
           [2,3,4,5],
           [2,3,4,5],
           [0,1,4,5],
           [0,1,4,5],
           [0,1,2,3],
           [0,1,2,3]
           ], dtype=np.int64)
       }
    
def change_o(data, o):
    xv = ORI['xs'][o%6,:]
    yv = ORI['xs'][ORI['ys'][o%6,o//6],:]
    zv = np.cross(xv, yv)
    rm = np.vstack((xv, yv, zv)).T
    #print(rm)
    return (rm @ data.T).T


def match_scanner_pair(id0, id1):
    s0 = scanners[id0]
    s1 = scanners[id1]
    
    matches = []
    s0l = s0.shape[0]
    s1l = s1.shape[0]
    m = -1
    for o in range(24):
        s1r = change_o(s1, o)
        
        for i in range(s0.shape[0]):
            ref0 = s0[i, :]
            for j in range(s1r.shape[0]):
                ref1 = s1r[j, :]
                
                dr = ref1 - ref0
                
                s1rr = s1r - [dr]
                
                ur, _, ur_cnt = np.unique(
                    np.vstack((s0, s1rr)), 
                    axis=0,
                    return_inverse=True, 
                    return_counts=True)
                
                m = s0l+s1l - ur.shape[0]
                
                if m >= 12:
                    # matches = [
                    #     -dr,
                    #     s1rr,
                    #     ur[ur_cnt==2, :]
                    #     ]
                    
                    break
            if m >= 12:
                break
        if m >= 12:
            break
    if m >= 12:
        return True, dr, s1rr, ur[ur_cnt==2, :]
    else:
        return False, None, None, None



nscan = len(scanners) 
scanner_pos = { 0: np.array([0,0,0], dtype=np.int64) }
correct = np.full((nscan,), False)
correct[0] = True

# %%

while not all(correct):    
    for a in range(nscan):
        for b in range(nscan):
            if correct[a] and not correct[b]:
                print(a,b)
                ab = match_scanner_pair(a, b)
                if ab[0]:
                    scanners[b] = ab[2]
                    #beacons.append(ab[3])
                    scanner_pos[b] = - ab[1]
                    correct[b] = True
            elif correct[b] and not correct[a]:
                print(b,a)
                ab = match_scanner_pair(b, a)
                if ab[0]:
                    scanners[a] = ab[2]
                    #beacons.append(ab[3])
                    scanner_pos[a] = - ab[1]
                    correct[a] = True
    print("{:d} correct, {:d} missing".format(
        correct.sum(), nscan-correct.sum())
        )
    
beacons = np.unique(np.vstack(scanners), axis=0) 

result = beacons.shape[0]

print('Puzzle solution: {:d}'.format(result))