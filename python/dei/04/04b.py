# -*- coding: utf-8 -*-
"""
Created on Sat Dec  4 07:00:18 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np
#import scipy.signal as ssig

# def main():  
    
    
fname = 'example.txt'
fname = 'input.txt'

bingo_numbers = pd.read_csv(
    fname, 
    nrows=1,
    header=None).iloc[0,:]

bingo_all_cards = pd.read_csv(
    fname, 
    skiprows=2, 
    delim_whitespace=True,
    header=None)

num_cards = bingo_all_cards.shape[0]//5

bingo_card_list = [
    bingo_all_cards.iloc[5*i:5*(i+1),:]
    for i in range(num_cards)
    ]

bingo_card_marks = [pd.DataFrame(np.full((5,5), False)) for c in range(num_cards)]

already_won = np.full((num_cards,), False)

is_bingo = lambda d: any(d.all(axis=0)) or any(d.all(axis=1))

for cur_draw in bingo_numbers:
    print(cur_draw)
    for cur_card in range(num_cards):
        if not already_won[cur_card]:
            y_pos, x_pos = np.nonzero(bingo_card_list[cur_card].to_numpy() == cur_draw)
            if len(x_pos)>0:
                x_pos = x_pos[0]
                y_pos = y_pos[0]
                bingo_card_marks[cur_card].iloc[y_pos, x_pos] = True

    check_res = np.array([is_bingo(bingo_card_marks[i]) and not already_won[i] for i in range(num_cards)])

    if any(check_res):
        print('BINGO')
        
        all_winner = np.nonzero(check_res)[0]
        
        for winner in all_winner:
            other_numbers = bingo_card_list[winner].to_numpy()[
                np.nonzero(bingo_card_marks[winner].to_numpy() == False)
                ]
        
            already_won[winner] = True
        
        if already_won.all():
            break
    
result = cur_draw * (other_numbers.sum())

# if __name__ == "__main__":
    # result = main()
print('Puzzle solution: {:d}'.format(result))
