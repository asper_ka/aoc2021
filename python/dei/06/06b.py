# -*- coding: utf-8 -*-
"""
Created on Mon Dec  6 05:58:23 2021

@author: PII_DEI
"""
import sys
import pandas as pd
import numpy as np

fname = 'input.txt'
ndays = 256
# fname = 'example.txt'
# ndays = 256

data = pd.read_csv(
    fname, 
    sep = ',', 
    header=None
    )

fishy = data.to_numpy()[0]

fishcat = np.zeros((9,), dtype=np.int64)

for n in range(9):
    fishcat[n] = (fishy == n).sum()

for days in range(ndays):
    num_new = fishcat[0]
    fishcat[:-1] = fishcat[1:]
    fishcat[6] += num_new
    fishcat[8] = num_new

print(fishcat)

result = fishcat.sum()

print('Puzzle solution: {:d}'.format(result))