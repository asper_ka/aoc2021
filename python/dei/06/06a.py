# -*- coding: utf-8 -*-
"""
Created on Mon Dec  6 05:58:23 2021

@author: PII_DEI
"""
import sys
import pandas as pd
import numpy as np

fname = 'input.txt'
ndays = 80
# fname = 'example.txt'
# ndays = 18

data = pd.read_csv(
    fname, 
    sep = ',', 
    header=None
    )

fishy = data.to_numpy()[0]

for days in range(ndays):
    spawny_fish = (fishy==0)
    
    fishy[~spawny_fish] -= 1
    fishy[spawny_fish] = 6
    
    num_new = spawny_fish.sum()
    if num_new > 0:
        new_fish = np.full((num_new,), 8, dtype=np.int64)      
        fishy = np.hstack((fishy, new_fish))        

# np.set_printoptions(threshold=sys.maxsize)
print(fishy)

result = fishy.size

print('Puzzle solution: {:d}'.format(result))