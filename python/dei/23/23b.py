# -*- coding: utf-8 -*-
"""
Created on Thu Dec 23 06:01:46 2021

@author: PII_DEI
"""

import numpy as np

costs = {
    'A': 1,
    'B': 10,
    'C': 100,
    'D': 1000
    }

goal = ['A', 'B', 'C', 'D']

fname = 'example.txt'
fname = 'input.txt'

with open(fname) as f:
    lines = f.read().splitlines()
lines.insert(3, '  #D#C#B#A#')
lines.insert(4, '  #D#B#A#C#')

apl = [
    [c for c in lines[2].replace('#','')] ,
    [c for c in lines[3].replace('#','').replace(' ','')],
    [c for c in lines[4].replace('#','').replace(' ','')] ,
    [c for c in lines[5].replace('#','').replace(' ','')]
    ]

rooms = np.array(list(map(list, zip(*apl))))

rdone = np.full((4,4), False)

hp = np.array([c for c in lines[1]])=='.'
nh = hp.sum()
hallway = np.full((nh,), '.')
hok = np.array([c for c in lines[2]])[hp]<'A'
room_entries = (np.nonzero(np.array([c for c in lines[2]])[hp]>='A')[0]).astype(np.uint64)
rlut = {room_entries[i]: i for i in range(len(rooms))}

hallway_entries2 = 2**np.array([0,1,7,2,7,3,7,4,7,5,6], dtype=np.uint64)
room_entries2 = 2**np.arange(8, 8+4*4, dtype=np.uint64).reshape((4,4)).T

for i in range(len(rooms)):
    for j in range(len(rooms[i])-1,-1,-1):
        if rooms[i][j] == goal[i]:
            print(goal[i], rooms[i][j])
            rdone[i][j] = True
        else:
            break
            
cur_state = {
    'energy': 0,
    'rooms': rooms,
    'hallway': hallway,
    'done': rdone,
    'dec': []
    }


all_solutions = {}

            
def s2id(cs):
    # mask = np.zeros((7,), dtype=np.uint64)
    # amps = np.zeros((1,), dtype=np.uint64)
    mask = hallway_entries2[cs['hallway'] > '.'].sum()
    mask += room_entries2[cs['rooms'] > '.'].sum()
    
    amps = np.zeros((1,), dtype=np.uint64)
    amps[0] += hallway_entries2[(cs['hallway'] == 'B') | (cs['hallway'] == 'D')].sum()
    amps[0] += room_entries2[(cs['rooms'] == 'B') | (cs['rooms'] == 'D')].sum()
    amps[0] += 2**24 * hallway_entries2[(cs['hallway'] == 'C') | (cs['hallway'] == 'D')].sum()
    amps[0] += 2**24 * room_entries2[(cs['rooms'] == 'C') | (cs['rooms'] == 'D')].sum()
    
    return mask, amps[0]
    
    
def cs_print(cs):
    print('E = {:6d}, MASK = 0x{:06X}, IDS = 0x{:012X}'.format(cs['energy'], *s2id(cs)))
    print(''.join(cs['hallway']))
    for j in range(4):
        s = [' '] * nh
        for i in range(4):
            s[room_entries[i]] = cs['rooms'][i,j]
        print(''.join(s))
    
            
def r2h_getmoves(cs, i = None):
    if i is None:
        res = np.array([], dtype=object)
        msks = np.array([], dtype=np.uint64)
        sids = np.array([], dtype=np.uint64)
        for i in range(len(cs['rooms'])):
            r, m, ids = r2h_getmoves(cs, i)
            res = np.hstack((res, r))
            msks = np.hstack((msks, m))
            sids = np.hstack((sids, ids))
            
        return res, msks, sids
    
    e = cs['energy']
    r = cs['rooms']
    h = cs['hallway']
    d = cs['done']
    dd = cs['dec']
    
    if d[i].all():
        return np.array([], dtype=object), np.array([], dtype=np.uint64), np.array([], dtype=np.uint64)
    
    iii = None
    steps = 0
    for ii in range(len(r[i])):
        if r[i][ii] >= 'A':
            if not d[i][ii]:
                iii = ii
            break
    if iii is None:
        return np.array([], dtype=object), np.array([], dtype=np.uint64), np.array([], dtype=np.uint64)
    
    steps = iii+1
    assert(steps>0)
    a = r[i][iii]
    
    res = []

    for j in range(room_entries[i], -1, -1):
        if not h[j] == '.':
            break
        
        if hok[j]:
            # empty space found
            hall_steps = room_entries[i] - j
            assert(hall_steps>0)
            cn =  {
                'energy': e,
                'rooms': r.copy(),
                'hallway': h.copy(),
                'done': d.copy(),
                'dec': dd.copy()
                }
            cn['rooms'][i,iii] = '.'
            cn['hallway'][j] = a
            cn['energy'] += ((steps+hall_steps)*costs[a])
            res.append(cn)
            
    for j in range(room_entries[i], len(h)):
        if not h[j] == '.':
            break
        
        if hok[j]:
            # empty space found
            hall_steps = j - room_entries[i]
            assert(hall_steps>0)
            cn =  {
                'energy': e,
                'rooms': r.copy(),
                'hallway': h.copy(),
                'done': d.copy(),
                'dec': dd.copy()
                }
            cn['rooms'][i,iii] = '.'
            cn['hallway'][j] = a
            cn['energy'] += ((steps+hall_steps)*costs[a])
            res.append(cn)
    m_i = [s2id(r) for r in res]
    return (np.array(res, dtype=object),
        np.fromiter((e[0] for e in m_i), dtype=np.uint64),      
        np.fromiter((e[1] for e in m_i), dtype=np.uint64))

def h2r_getmoves(cs, j = None):    
    if j is None:
        res = np.array([], dtype=object)
        msks = np.array([], dtype=np.uint64)
        sids = np.array([], dtype=np.uint64)
        h = cs['hallway']
        for j in range(len(h)):
            if h[j] >= 'A':
                r, m, ids = h2r_getmoves(cs, j)
                res = np.hstack((res, r))
                msks = np.hstack((msks, m))
                sids = np.hstack((sids, ids))
        return res, msks, sids
    
    h = cs['hallway']
    e = cs['energy']
    r = cs['rooms']
    d = cs['done']
    dd = cs['dec']
    
    res = []
    a = h[j]
    
    for jj in range(j-1, -1, -1):
        if h[jj] >= 'A':
            break
        if not hok[jj]: # found room
            i = rlut[jj]
            if (goal[i] != a) or (r[i]>='A').all():
                continue # wrong room or full
                
            for ii in range(len(r[i])-1, -1, -1):
                if r[i][ii] == '.':
                    # empty room space found
                    steps = (j-jj+1) + ii
                    assert(steps>0)
                    cn =  {
                        'energy': e,
                        'rooms': r.copy(),
                        'hallway': h.copy(),
                        'done': d.copy(),
                        'dec': dd.copy()
                        }
                    cn['rooms'][i,ii] = a
                    cn['hallway'][j] = '.'
                    cn['energy'] += steps*costs[a]
                    cn['done'][i,ii] = True
                    res.append(cn)
                    break
                elif r[i][ii] != a:
                    # rule 2 => abort if wrong amphipods are present
                    break 

    for jj in range(j+1, len(hallway)):
        #print(jj)
        if h[jj] >= 'A':
            break
        #print('-')
        if not hok[jj]: # found room
            #print('--')
            i = rlut[jj]
            if (goal[i] != a) or (r[i]>='A').all():
                continue # wrong room or full
            for ii in range(len(r[i])-1, -1, -1):
                if r[i][ii] == '.':
                    # empty room space found
                    steps = (jj-j+1) + ii
                    assert(steps>0)
                    cn =  {
                        'energy': e,
                        'rooms': r.copy(),
                        'hallway': h.copy(),
                        'done': d.copy(),
                        'dec': dd.copy()
                        }
                    cn['rooms'][i,ii] = a
                    cn['hallway'][j] = '.'
                    cn['energy'] += steps*costs[a]
                    cn['done'][i,ii] = True
                    res.append(cn)
                    break
                elif r[i][ii] != a:
                    # rule 2 => abort if wrong amphipods are present
                    break 
                    
    m_i = [s2id(r) for r in res]
    return (np.array(res, dtype=object),
        np.fromiter((e[0] for e in m_i), dtype=np.uint64),      
        np.fromiter((e[1] for e in m_i), dtype=np.uint64))


def get_next_options(cs):
    if isinstance(cs, dict):
        x, xm, xids = r2h_getmoves(cs)
        y, ym, yids = h2r_getmoves(cs)
        
        res = np.hstack((x, y))
        msks = np.hstack((xm, ym))
        sids = np.hstack((xids, yids))
    else:
        res = np.array([], dtype=object)
        msks = np.array([], dtype=np.uint64)
        sids = np.array([], dtype=np.uint64)
        for s in cs:
            r, m, ids = get_next_options(s)
            res =  np.hstack((res, r))
            msks =  np.hstack((msks, m))
            sids = np.hstack((sids, ids))
            
    msids = np.vstack((msks, sids)).T
                
    _, idz = np.unique(msids, axis=0, return_index=True)
    
    #res = res[idz]
    newres = []
    newmsks = []
    newids = []
    
    j = 0
    for i in idz:
        s = res[i]
        min_energy = np.fromiter(
            (r['energy'] for r in res[(sids == sids[i]) & (msks == msks[i])]),
            dtype=np.uint64).min()
        # if s['energy'] != min_energy:
        #     print(s)
        s['energy'] = min_energy 
        s['dec'].append(j)
        
        if s['done'].all():
            if not s['energy'] in all_solutions:
                all_solutions[s['energy']] = s
        else:
            newres.append(s)
            newmsks.append(msks[i])
            newids.append(sids[i])
        
        j += 1
    
    return np.array(newres, dtype=object), np.array(newmsks, dtype=np.uint64), np.array(newids, dtype=np.uint64)

# %%
csl = [cur_state]
# uid = np.fromiter((s2id(c) for c in csl), dtype=np.uint64)
while len(csl) > 0:
    print(len(csl))
    csl, _, _ = get_next_options(csl)

# def r2h(stack_id):
#     if 



result = np.array(list(all_solutions.keys())).min()

print('Puzzle solution: {:d}'.format(result))