# -*- coding: utf-8 -*-
"""
Created on Mon Dec 20 22:12:07 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np
import scipy.signal as ssig

fname = 'example.txt'
fname = 'input.txt'

with open(fname) as f:
    lines = f.read().splitlines()

iea = np.array([1 if c=='#' else 0 for c in lines[0]], dtype=np.int64)


iimg = np.array([[1 if c=='#' else 0 for c in l] for l in lines[2:]], dtype=np.int64)

nx = 10

eiimg = np.zeros((iimg.shape[0]+2*nx, iimg.shape[1]+2*nx), dtype=np.int64)
eiimg[nx:-nx, nx:-nx] = iimg

result = 0

flt = np.array([
    [256,128,64],
    [32,16,8],
    [4,2,1]
    ], dtype=np.int64)

cimg = eiimg.copy()

for i in range(50):
    fv = cimg[0,0]
    cimg = ssig.correlate2d(cimg, flt, mode='full', fillvalue=fv).copy()
    cimg = iea[cimg].copy()
    
result = cimg.sum()

print('Puzzle solution: {:d}'.format(result))