# -*- coding: utf-8 -*-
"""
Created on Mon Dec 13 05:58:41 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np

fname = 'example.txt'
fname = 'input.txt'

data = pd.read_csv(
    fname, 
    sep = ',', 
    header=None
    )

numf = data[0].apply(lambda s: s[0]=='f').sum()

data = pd.read_csv(
    fname, 
    sep = ',', 
    header=None, skipfooter=numf+1, engine='python'
    )

comms = pd.read_csv(
    fname, 
    sep = '=', 
    header=None, skiprows=data.shape[0]+1
    )
comms[0] = comms[0].apply(lambda s: s[-1])

xmax = data[0].max()
ymax = data[1].max()

for i in range(comms.shape[0]):
    if comms.loc[i, 0] == 'x':
        fx = comms.loc[i, 1]
        data.loc[data[0] > fx, 0] = fx - (data.loc[data[0] > fx, 0] - fx)
    else:
        fy = comms.loc[i, 1]
        data.loc[data[1] > fy, 1] = fy - (data.loc[data[1] > fy, 1] - fy)

data = data.drop_duplicates()

result = data.shape[0]

print('Puzzle solution: {:d}'.format(result))