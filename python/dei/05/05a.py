# -*- coding: utf-8 -*-
"""
Created on Sun Dec  5 06:02:36 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np


data = pd.read_csv(
    'input.txt', 
    sep = ' -> |,', 
    engine='python',
    header=None
    )

nsize = data.max(axis=0).max()+1

diag = pd.DataFrame(
    np.zeros((nsize, nsize), dtype=np.int64)
    )

for i in range(data.shape[0]):
    if data.iloc[i,0] == data.iloc[i,2]:
        a = data.iloc[i,1]
        b = data.iloc[i,3]
        aa = min(a,b)
        bb = max(a,b)
        x = data.iloc[i,0]
        for y in range(aa, bb+1):
            diag.iloc[x, y] += 1
            
    elif data.iloc[i,1] == data.iloc[i,3]:
        a = data.iloc[i,0]
        b = data.iloc[i,2]
        aa = min(a,b)
        bb = max(a,b)
        y = data.iloc[i,1]
        for x in range(aa, bb+1):
            diag.iloc[x, y] += 1

print((diag >= 2).sum().sum())
print(diag)