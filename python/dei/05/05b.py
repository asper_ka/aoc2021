# -*- coding: utf-8 -*-
"""
Created on Sun Dec  5 06:02:36 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np


data = pd.read_csv(
    'input.txt', 
    sep = ' -> |,', 
    engine='python',
    header=None
    )

nsize = data.max(axis=0).max()+1


dxdata = data[2]-data[0]
dydata = data[3]-data[1]

x1data = data[0]
y1data = data[1]

vdata = data.loc[(dxdata == 0)].copy()
hdata = data.loc[(dydata == 0)].copy()

vdata['start'] = vdata[[1,3]].min(axis=1).to_numpy()
vdata['end'] = 1 + vdata[[1,3]].max(axis=1).to_numpy()

hdata['start'] = hdata[[0,2]].min(axis=1).to_numpy()
hdata['end'] = 1 + hdata[[0,2]].max(axis=1).to_numpy()


ddata = data.loc[(dxdata != 0) & (dydata != 0)].copy()
ddata['xdir'] = np.sign(ddata[2]-ddata[0])
ddata['ydir'] = np.sign(ddata[3]-ddata[1])
ddata['llen'] = 1 + (ddata[[0,2]].max(axis=1)-ddata[[0,2]].min(axis=1)).to_numpy()

# %%

def drawvline(r):
    for y in range(r.start, r.end):
        diag.iloc[y, r[0]] += 1

def drawhline(r):
    for x in range(r.start, r.end):
        diag.iloc[r[1], x] += 1
    
def drawdline(r):
    x = r[0]
    y = r[1]
    for i in range(r.llen):
        diag.iloc[y, x] += 1
        x += r.xdir
        y += r.ydir

diag = pd.DataFrame(
    np.zeros((nsize, nsize), dtype=np.int64)
    )

vdata.apply(drawvline, axis=1)
hdata.apply(drawhline, axis=1)
ddata.apply(drawdline, axis=1)

print(diag)
print((diag >= 2).sum().sum())
