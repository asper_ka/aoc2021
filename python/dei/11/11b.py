# -*- coding: utf-8 -*-
"""
Created on Sat Dec 11 05:58:12 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np

fname = 'example0.txt'
fname = 'example.txt'
fname = 'input.txt'

data = pd.read_csv(
    fname, 
    sep = ' ', 
    header=None,
    dtype='str'
    )

data = data[0].to_list()

w = len(data[0])
h = len(data)

data = np.array([[int(d) for d in r] for r in data])

flashed_0 = np.full(data.shape, False)

flashes = 0

all_flash = False
iter = 0

while not all_flash:
    flashed = flashed_0.copy()
    data += 1
    
    to_flash = (data > 9) & (~flashed)
    while to_flash.any():
        y, x = np.nonzero(to_flash)
        
        for dy in [-1,0,1]:
            for dx in [-1,0,1]:
                if (dx==0) and (dy==0):
                    continue
                
                yy = y+dy
                xx = x+dx
                valid = (xx>=0) & (yy>=0) & (xx<w) & (yy<h)
                xx = xx[valid]
                yy = yy[valid]
                data[yy, xx] += 1
                
        flashes += to_flash.sum().sum()
        flashed |= to_flash    
        to_flash = (data > 9) & (~flashed)
    
    data[flashed] = 0
    iter += 1
    print("{:d}: {:d}".format(iter, flashed.sum().sum()))
    all_flash = flashed.sum().sum() == w*h

result = iter

print('Puzzle solution: {:d}'.format(result))