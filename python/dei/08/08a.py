# -*- coding: utf-8 -*-
"""
Created on Wed Dec  8 05:59:27 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np

fname = 'example.txt'
fname = 'example2.txt'
fname = 'input.txt'

data = pd.read_csv(
    fname, 
    sep = ' ', 
    header=None
    )

#del data[10]

digs = pd.DataFrame([data.loc[:, i].apply(len) for i in range(11,15)]).T

simple_count = ((digs==2)|(digs==4)|(digs==3)|(digs==7)).sum().sum()


result = simple_count

print('Puzzle solution: {:d}'.format(result))