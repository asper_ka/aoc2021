# -*- coding: utf-8 -*-
"""
Created on Wed Dec  8 05:59:27 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np

fname = 'example.txt'
fname = 'example2.txt'
fname = 'input.txt'

data = pd.read_csv(
    fname, 
    sep = ' ', 
    header=None
    )

#del data[10]

def s_to_num(s):
    v = ((1 if 'a' in s else 0) + 
        (2 if 'b' in s else 0) + 
        (4 if 'c' in s else 0) + 
        (8 if 'd' in s else 0) + 
        (16 if 'e' in s else 0) + 
        (32 if 'f' in s else 0) + 
        (64 if 'g' in s else 0))
    return v

digs_bcnt = pd.DataFrame([data.loc[:, i].apply(len) for i in range(0,10)]).T
digs_base = pd.DataFrame([data.loc[:, i].apply(s_to_num) for i in range(0,10)]).T


digs_disp = pd.DataFrame([data.loc[:, i].apply(s_to_num) for i in range(11,15)]).T

digs_bcnt_np = digs_bcnt.to_numpy()
digs_base_np = digs_base.to_numpy()
digs_disp_np = digs_disp.to_numpy()

ones = digs_base_np[digs_bcnt_np == 2]
fours = digs_base_np[digs_bcnt_np == 4]
sevens = digs_base_np[digs_bcnt_np == 3]
eights = digs_base_np[digs_bcnt_np == 7]

a_v = np.bitwise_xor(ones, sevens)

fives = np.zeros(ones.shape, dtype=np.int64)
zeros = np.zeros(ones.shape, dtype=np.int64)

twos = np.zeros(ones.shape, dtype=np.int64)
threes = np.zeros(ones.shape, dtype=np.int64)

nines = np.zeros(ones.shape, dtype=np.int64)
sixes = np.zeros(ones.shape, dtype=np.int64)

for i in range(data.shape[0]):
    cf_v5 = np.bitwise_xor(ones, fours)[i]
    five_test = np.bitwise_and(digs_base_np[i, digs_bcnt_np[i,:]==5], cf_v5)==cf_v5
    five_id = (np.nonzero(digs_bcnt_np[i,:]==5)[0])[five_test]
    fives[i] = digs_base_np[i, five_id]


    cf_v0 = np.bitwise_xor(fives, eights)[i]
    zero_test = np.bitwise_and(digs_base_np[i, digs_bcnt_np[i,:]==6], cf_v0)==cf_v0
    zero_id = (np.nonzero(digs_bcnt_np[i,:]==6)[0])[zero_test]
    zeros[i] = digs_base_np[i, zero_id]
    
    
    comp23 = digs_base_np[i,(np.nonzero(digs_bcnt_np[i,:]==5)[0])[~five_test]]
    ca23 = comp23[0] & comp23[1]
    comp23 = np.bitwise_xor(comp23, ca23)
    three_test = (comp23 & cf_v0)==0
    two_test = (comp23 & cf_v0)!=0
    two_three_id = (np.nonzero(digs_bcnt_np[i,:]==5)[0])[~five_test]
    twos[i] = digs_base_np[i, two_three_id[two_test]]
    threes[i] = digs_base_np[i, two_three_id[three_test]]
    
    g = twos[i] ^ (twos[i] & threes[i])
    six_nine_id = (np.nonzero(digs_bcnt_np[i,:]==6)[0])[~zero_test]
    six_test = (digs_base_np[i, six_nine_id] & g) != 0
    nine_test = (digs_base_np[i, six_nine_id] & g) == 0
    sixes[i] = digs_base_np[i, six_nine_id[six_test]]
    nines[i] = digs_base_np[i, six_nine_id[nine_test]]
    

dig_lut = np.array([
    zeros, ones, twos, threes, fours, fives, sixes, sevens, eights, nines]).T

dig_sol = np.zeros( (data.shape[0],), dtype=np.int64)

for i in range(data.shape[0]):
    v = 0
    for j in range(4):
        v *= 10
        v += np.nonzero(digs_disp_np[i,j] == dig_lut[i])[0]
    dig_sol[i] = v
    print(v)


result = dig_sol.sum()

print('Puzzle solution: {:d}'.format(result))