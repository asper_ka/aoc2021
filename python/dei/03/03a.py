# -*- coding: utf-8 -*-
"""
Created on Fri Dec  3 05:55:58 2021

@author: PII_DEI
"""

import pandas as pd

def main():
    gamma = ''
    epsilon = ''
    
    data = pd.read_csv(
        'input.txt', 
        dtype=object,
        header = None)
    
    slen = len(data.iloc[0,0])
    
    for i in range(slen):
        cnt1 = data[0].apply(lambda v: v[i]).astype(int).sum()
        if cnt1 > data.shape[0]/2:
            gamma += '1'
            epsilon += '0'
        else:
            gamma += '0'
            epsilon += '1'
    
    gamma = int(gamma,2)
    epsilon = int(epsilon,2)
    
    return gamma * epsilon

if __name__ == "__main__":
    result = main()
    print('Puzzle solution: {:d}'.format(result))
