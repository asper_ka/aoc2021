# -*- coding: utf-8 -*-
"""
Created on Fri Dec  3 05:55:58 2021

@author: PII_DEI
"""

import pandas as pd

def main():    
    oxygen = ''
    co2 = ''
    
    data = pd.read_csv(
        'input.txt', 
        dtype=object,
        header = None)
    
    slen = len(data.iloc[0,0])
    
    odata = data.copy()
    cdata = data.copy()
    
    for i in range(slen):
        if odata.shape[0]>1:
            ocnt = odata[0].apply(lambda v: v[i]).astype(int).sum()
            oval = '1' if ocnt >= odata.shape[0]/2 else '0'
            odata = odata.loc[odata[0].apply(lambda v: v[i] == oval)]
        
        if cdata.shape[0]>1:
            ccnt = cdata[0].apply(lambda v: v[i]).astype(int).sum()
            cval = '1' if ccnt < cdata.shape[0]/2 else '0'
            cdata = cdata.loc[cdata[0].apply(lambda v: v[i] == cval)]
    
    oxygen = int(odata.iloc[0,0],2)
    co2 = int(cdata.iloc[0,0],2)
    
    return oxygen * co2
        

if __name__ == "__main__":
    result = main()
    print('Puzzle solution: {:d}'.format(result))
