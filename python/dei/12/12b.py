# -*- coding: utf-8 -*-
"""
Created on Sun Dec 12 06:00:16 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np

fname = 'example1.txt'
fname = 'example2.txt'
fname = 'example3.txt'
fname = 'input.txt'

data = pd.read_csv(
    fname, 
    sep = '-', 
    header=None,
    dtype='str'
    )

caves = np.unique(np.hstack((data[0].unique(),data[1].unique())))
caves.sort()

cisbig = np.array([c.isupper() and c !='start' and c != 'end' for c in caves])
cissmall = np.array([c.islower() and c !='start' and c != 'end' for c in caves])

flt = cisbig | cissmall
caves = caves[flt].copy()
cisbig = cisbig[flt].copy()
cissmall = cissmall[flt].copy()

conns = data.to_numpy()

start_conns = np.hstack((
    conns[conns[:,0] == 'start', 1],
    conns[conns[:,1] == 'start', 0]))


end_conns = np.hstack((
    conns[conns[:,0] == 'end', 1],
    conns[conns[:,1] == 'end', 0]))

t = ((conns != 'start') & (conns != 'end')).all(axis=1)
other_conns = conns[t,:]
other_conns =np.vstack((other_conns, other_conns[:, [1,0]]))


numcaves = len(caves)
for i in range(numcaves):
    other_conns[other_conns == caves[i]] = i
    start_conns[start_conns == caves[i]] = i
    end_conns[end_conns == caves[i]] = i

other_conns = other_conns.astype(int)
start_conns = start_conns.astype(int)
end_conns = end_conns.astype(int)
paths = []



def traverse(p):
    global visited, curpath
    
    curpath.append(p)
    visited[p] += 1
    
    nextcaves = other_conns[other_conns[:,0]==p, 1]
    nextcaves.sort()
        
    # print('start,' + ','.join(caves[np.array(curpath)]) + ': {:s}'.format(
    #     ', '.join(['{:s}={:d}'.format(caves[i], visited[i]) for i in range(numcaves)]) ))
    
    had_twice = ((cissmall) & (visited==2)).any()
    
    if had_twice:
        nextcaves = nextcaves[cisbig[nextcaves] | (visited[nextcaves]==0)]
    else:
        nextcaves = nextcaves[cisbig[nextcaves] | (visited[nextcaves]<=1)]
    
    for nc in nextcaves:
        traverse(nc)
    
    if p in end_conns:
        print('start,' + ','.join(caves[np.array(curpath)]) + ',end')
        # print("===============================================")
        paths.append(np.array(curpath).copy())
        
    visited[p] -= 1
    curpath.pop()
    
        

for sc in start_conns: 
    visited = np.full(caves.shape, 0)
    curpath = []    
    traverse(sc)
        
    


result = len(paths)

print('Puzzle solution: {:d}'.format(result))