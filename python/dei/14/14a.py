# -*- coding: utf-8 -*-
"""
Created on Tue Dec 14 05:59:22 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np

fname = 'example.txt'
fname = 'input.txt'


data1 = pd.read_csv(
    fname, 
    sep = ';', 
    dtype = 'str',
    header=None, 
    engine='python',
    nrows = 1
    )

chain = np.array([c for c in data1.iloc[0,0]])
    
lut = pd.read_csv(
    fname, 
    sep = ' -> ', 
    header=None, 
    engine='python',
    skiprows=2
    )

lut = lut.to_numpy()

for iter in range(10):
    pairs = [chain[i]+chain[i+1] for i in range(len(chain)-1)]
    
    newins = [lut[lut[:,0] == p, 1][0] for p in pairs]
    newins.append('')
    
    chain = np.vstack([chain, newins]).T.reshape((len(chain)*2,))[:-1]

elements = np.unique(chain)

counts = {}
for e in elements:
    counts[e] = (chain == e).sum()
    
vals = np.array(list(counts.values()))
    
result = vals.max() - vals.min()

print('Puzzle solution: {:d}'.format(result))