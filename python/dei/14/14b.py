# -*- coding: utf-8 -*-
"""
Created on Tue Dec 14 05:59:22 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np

fname = 'example.txt'
fname = 'input.txt'


data1 = pd.read_csv(
    fname, 
    sep = ';', 
    dtype = 'str',
    header=None, 
    engine='python',
    nrows = 1
    )

chain = np.array([c for c in data1.iloc[0,0]])
    
lut = pd.read_csv(
    fname, 
    sep = ' -> ', 
    header=None, 
    engine='python',
    skiprows=2
    )

lut = lut.to_numpy()

lutnew = np.array([[
    lut[i,0], 
    lut[i,0][0]+lut[i,1], 
    lut[i,1]+lut[i,0][1]] for i in range(lut.shape[0])])

idxlut = np.array([[
    i,
    np.nonzero(lutnew[:,0]==lutnew[i,1])[0][0],
    np.nonzero(lutnew[:,0]==lutnew[i,2])[0][0]] for i in range(lutnew.shape[0])])


pairs = [chain[i]+chain[i+1] for i in range(len(chain)-1)]

pairs_cnt = np.zeros((lut.shape[0],), dtype=np.int64)
for p in pairs:
    pairs_cnt[lut[:,0] == p] += 1


for iter in range(40):
    new_pairs_cnt = np.zeros(pairs_cnt.shape, dtype=np.int64)
    
    for i in range(pairs_cnt.size):
        new_pairs_cnt[idxlut[i, 1]] += pairs_cnt[i]
        new_pairs_cnt[idxlut[i, 2]] += pairs_cnt[i]
    
    pairs_cnt = new_pairs_cnt

el_lut = np.array([[lut[i,0][0], lut[i,0][1]] for i in range(lut.shape[0])])

el_lst = np.unique(el_lut)

cnt_lut = np.array([
    1*(el_lst == el_lut[i, 0]) + 1*(el_lst == el_lut[i, 1]) 
    for i in range(lut.shape[0])])

el_cnt = np.array(
    [pairs_cnt * cnt_lut[:,i] for i in range(el_lst.size)]).T.sum(axis=0)    

first_last = (1*(el_lst == chain[0]) + 1*(el_lst == chain[-1]))
el_cnt = (el_cnt - first_last)//2 + first_last

result = el_cnt.max() - el_cnt.min()

print('Puzzle solution: {:d}'.format(result))