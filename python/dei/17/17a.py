# -*- coding: utf-8 -*-
"""
Created on Fri Dec 17 06:01:33 2021

@author: PII_DEI
"""

import pandas as pd
import numpy as np

fname = 'input.txt'
fname = 'example.txt'



data = pd.read_csv(
    fname, 
    sep = ' ', 
    header=None,
    dtype='str'
    ).iloc[0, [-2,-1]]

dataa = data.apply(lambda s: int(s.split('=')[1].split('..')[0])).to_numpy()
datab = data.apply(lambda s: s.split('=')[1].split('..')[1]).to_numpy()
datab[0] = datab[0][:-1]
datab=datab.astype(int)

x1 = dataa[0]
y1 = dataa[1]

x2 = datab[0]
y2 = datab[1]



# vy max = y2
# vx min = (x1)   vx*vx + vx - 2*x1 == 0
vymax = -y1
vxmin = int(np.ceil(-1/2 + np.sqrt(1/4 + 2*x1)))
vxmax = x2+1
vymin = 0

sol = []
for vy in range(vymin, vymax):
    for vx in range(vxmin, vxmax):
        x = 0
        y = 0
        maxy = 0
        cvx = vx
        cvy = vy
        while True:
           y += cvy
           x += cvx
           cvy -= 1
           if cvx > 0:
               cvx -= 1
           
           if cvy == 0:
               maxy = y
           
           if (x>=x1) and (x<=x2) and (y>=y1) and (y<=y2):
               sol.append([vx, vy, maxy])
               
           if x>x2 or y<y1:
               break
sol = np.array(sol)

print(sol[:,2].max())