# -*- coding: utf-8 -*-
"""
Created on Fri Dec 24 05:54:07 2021

@author: PII_DEI
"""

import numpy as np


fname = 'example1.txt'
inp_buf = '4'


fname = 'example2.txt'
inp_buf = '39'
inp_buf = '38'


fname = 'example3.txt'
inp_buf = '13'
    
alu_vars = {
    'w': 0,
    'x': 0,
    'y': 0,
    'z': 0
    }


fname = 'input.txt'

def alu_reset():
    global alu_vars
    alu_vars['w'] = 0
    alu_vars['x'] = 0
    alu_vars['y'] = 0
    alu_vars['z'] = 0

def op_inp(a, b=None):
    global inp_buf
    s = int(inp_buf[0])
    alu_vars[a] = s
    inp_buf = inp_buf[1:]
    
def op_add(a, b):
    if isinstance(b, int):
        alu_vars[a] += b
    else:
        alu_vars[a] += alu_vars[b]
    
def op_mul(a, b):
    if isinstance(b, int):
        alu_vars[a] *= b
    else:
        alu_vars[a] *= alu_vars[b]
    
def op_div(a, b):
    if isinstance(b, int):
        v = alu_vars[a] // b
        # try:
        #     alu_vars[a] = int(alu_vars[a] / b)
        # except OverflowError as e:
        #     print(e)
    else:
        # alu_vars[a] = int(alu_vars[a] / alu_vars[b])
        v = alu_vars[a] // alu_vars[b]
    v = v+1 if v<0 else v
    alu_vars[a] = v

def op_mod(a, b):
    if isinstance(b, int):
        alu_vars[a] = alu_vars[a] % b
    else:
        alu_vars[a] = alu_vars[a] % alu_vars[b]
        
def op_eql(a, b):
    if isinstance(b, int):
        alu_vars[a] = 1 if alu_vars[a] == b else 0
    else:
        alu_vars[a] = 1 if alu_vars[a] == alu_vars[b] else 0
    
alu_ops = {
    'inp': op_inp,
    'add': op_add,
    'mul': op_mul,
    'div': op_div,
    'mod': op_mod,
    'eql': op_eql    
    }

def exec_op(op, a, b):
    alu_ops[op](a, b)
    
def exec_code(c):
    for cl in c:
        exec_op(*cl)

    
with open(fname) as f:
    lines = f.read().splitlines()

code_lines = [l.split(' ') for l in lines]

code_ops = [cl[0] for cl in code_lines]

code_v1 = [cl[1] for cl in code_lines]

code_v2 = ['' if len(cl)<3 else (int(cl[-1]) if not cl[-1] in ['x','y','z','w'] else cl[-1]) for cl in code_lines]

code = [[code_ops[i], code_v1[i], code_v2[i]] for i in range(len(code_ops))]

# exec_code(code)


def dec_model_number(n):
    i = len(n)-1
    
    while i>=0:
        n[i] -= 1
        if n[i] > 0:
            return n
        n[i] = 9
        i -= 1
    
    return n

def set_buf(n):
    global inp_buf
    s = ''.join([str(v) for v in n])
    # print(s)
    inp_buf = s

inp_lineno = np.nonzero(np.array([c == 'inp' for c in code_ops]))[0]
num_blocks = inp_lineno.size
inp_lineno = np.append(inp_lineno, len(code))

zranges = [set() for z in range(15)]
zranges[0].add(0)

zz = 11

for b in range(zz):
    print('Forward search {:d}'.format(b))
    for z in zranges[b]:
        for d in range(1,10):
            alu_reset()
            alu_vars['z'] = z
            set_buf([d])
            exec_code(code[inp_lineno[b]:inp_lineno[b+1]])
            zranges[b+1].add(alu_vars['z'])
    print('Cases: {:d}'.format(len(zranges[b+1])))
    
    
zranges2 = [set() for z in range(15)]
zranges2[14].add(0)



for b in range(13,zz-1,-1):
    print('Reverse search {:d}'.format(b))
    for z in range(-30000, 30000):
        for d in range(1,10):
            alu_reset()
            alu_vars['z'] = z
            set_buf([d])
            exec_code(code[inp_lineno[b]:inp_lineno[b+1]])
            # print(d, z, alu_vars['z'])
            if alu_vars['z'] in zranges2[b+1]:
                zranges2[b].add(z)
    if len(zranges2[b]) == 0:
        print('NO SOLUTION!!!')
        break
    else:
        print('Cases: {:d}'.format(len(zranges2[b])))
        
zranges[zz] = zranges2[zz].intersection(zranges[zz])
print('Reduced forward cases:', len(zranges[zz]))

for b in range(zz, 14):
    print('Forward search {:d}'.format(b))
    for z in zranges[b]:
        for d in range(1,10):
            alu_reset()
            alu_vars['z'] = z
            set_buf([d])
            exec_code(code[inp_lineno[b]:inp_lineno[b+1]])
            zranges[b+1].add(alu_vars['z'])
            
    zranges[b+1] = zranges2[b+1].intersection(zranges[b+1])
    print('Cases: {:d}'.format(len(zranges[b+1])))


zranges3 = [set() for z in range(15)]
for b in range(zz, 15):
    zranges3[b] = zranges[b]

for b in range(zz-1, -1, -1):
    print('Backward search {:d}'.format(b))
    for z in zranges[b]:
        for d in range(9,0,-1):
            alu_reset()
            alu_vars['z'] = z
            set_buf([d])
            exec_code(code[inp_lineno[b]:inp_lineno[b+1]])
            if alu_vars['z'] in zranges3[b+1]:
                zranges3[b].add(z)
                
    print('Cases: {:d}'.format(len(zranges3[b+1])))

sols = [{0: 0}]
for b in range(14):
    sols.append({})
    print('Calc solution at {:d}'.format(b))
    for z in zranges3[b]:
        for d in range(9,0,-1):
            alu_reset()
            alu_vars['z'] = z
            set_buf([d])
            exec_code(code[inp_lineno[b]:inp_lineno[b+1]])
            if alu_vars['z'] in zranges3[b+1]:
                v = sols[b][z] * 10 + d
                if (alu_vars['z'] in sols[b+1]):
                    v2 = sols[b+1][alu_vars['z']]
                else:
                    v2 = -1
                
                if v > v2:
                    sols[b+1][alu_vars['z']] = v
            
    print('done...')

result = sols[-1]

print('Puzzle solution: {:d}'.format(result))