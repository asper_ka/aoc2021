# -*- coding: utf-8 -*-
"""
Created on Sat Dec 25 06:07:18 2021

@author: PII_DEI
"""

import numpy as np

fname = 'example1.txt'
fname = 'example2.txt'
fname = 'input.txt'

with open(fname) as f:
    lines = f.read().splitlines()

herd_x = np.array([[c=='>' for c in l] for l in lines],dtype=int)
herd_y = np.array([[c=='v' for c in l] for l in lines],dtype=int)

def move_x(m, dir=1):
    if dir > 0:
        return np.hstack((m[:, [-1]], m[:, :-1]))
    else:
        return np.hstack((m[:, 1:], m[:, [0]]))
    
def move_y(m, dir=1):
    if dir > 0:
        return np.vstack((m[[-1], :], m[:-1, :]))
    else:
        return np.vstack((m[1:, :], m[[0], :]))
    
def h_print(hx, hy):
    print('\n'.join([''.join(['>' if v==1 else 'v' if v==2 else '.' for v in l]) for l in hx + 2*hy]))

    
steps = 0

nmoved = 1

while nmoved > 0:
    nx = move_x(herd_x, 1)
    ocp = herd_x | herd_y
    nx_m = nx & ~ocp
    nx_s = move_x(nx & ocp, -1)
    herd_x = nx_m | nx_s
    moved_x = nx_m.sum()
    
    ny = move_y(herd_y, 1)
    ocp = herd_x | herd_y
    ny_m = ny & ~ocp
    ny_s = move_y(ny & ocp, -1)
    herd_y = ny_m | ny_s
    moved_y = ny_m.sum()

    steps += 1
    nmoved = moved_x + moved_y
    
result = steps

h_print(herd_x, herd_y)

print('Puzzle solution: {:d}'.format(result))